#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append('../../../lib/src/.libs') # remove when installed
from pygtx import MainLoop, MainInit, Frame, Label, setLabelText, List,\
                  addItem, getSelectedItem, ComboList, getComboListText

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

notifylabel = 0
data = []

def ListCallback(w, id, data):
  item, index = getSelectedItem(w)
  if id == 'combolist':
    text = getComboListText(w, 50)
    print('Combo list box      : %s' % id)
    print('Selected combo index: %d' % index)
    print('Selected combo item : %s' % item)
    print('Selected combo text : %s' % text)
    setLabelText(notifylabel, 'Combo list clicked: %s' % text)
  else:
    print('List box            : %s' % id)
    print('Selected item index : %d' % index)
    print('Selected list item  : %s' % item)
    setLabelText(notifylabel, 'List clicked: %s' % item)
  # Unselection event also notified with text NIL 
  print('Widget#      : %d' % w)
  print('Data.ncontrol: %d' % data.ncontrol)
  print('Data.comment : %s' % data.comment)

# Create main windows and initialize toolkit 
main = MainInit('List', 'Demo list and combo box', True)

# Add  a frame of result 
frame1 = Frame(main, 'Last item selected', 400)

# Add an instruction label on top of the main window 
notifylabel = Label(frame1, 'Please, select an item from the list or combo')

# Add frame for listbox 
frame2 = Frame(main, 'Please, click on the list', 400)

# Add listbox 
data.append(Data(401, 'List box with simple selection'))
list = List(frame2, 1, ListCallback, data[len(data) - 1])
addItem(list, 'First item with a long line')
addItem(list, 'Second item with a long long line')
addItem(list, 'Third item is a good item')
addItem(list, 'Fourth item is not a big deal')
addItem(list, 'Fifth item is easy and ready')
addItem(list, 'Sixth item and other things')
addItem(list, 'Seventh item is nearly out')
addItem(list, 'Eighth item with all nearly done')
addItem(list, 'Nineth item and last, uf!')

# Add frame for combobox 
frame3 = Frame(main, 'Select an item from the combobox', 400)

# Add combolist with prompt label 
data.append(Data(402, 'Combo list box with simple selection'))
combolist = ComboList(frame3, 225, 50, ListCallback, data[len(data) - 1])
addItem(combolist, 'First item of combolist')
addItem(combolist, 'Second item of combolist')
addItem(combolist, 'Third item of combolist')
addItem(combolist, 'Fourth item of combolist')

# Main event dispatching loop 
MainLoop()
