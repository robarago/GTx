#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append("../../../lib/src/.libs") # remove when installed
from pygtx import MainLoop, MainInit, Box, Dialog, InputDialog, closeDialog,\
                  FileDialog, Button, MenuItem, Popup, Separator,\
                  getDialogSelection, DIALOG_OPEN, DIALOG_SAVE, HORIZONTAL,\
                  DIALOG_QUESTION, DIALOG_INFO, BUTTON_YES, BUTTON_NO, BUTTON_OK

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

notifylabel = 0
data = []

def ExitCallback (w, id, data):
  if id == "Yes":
    exit(0)
  else:
    closeDialog(w)

def InputDialogCallback (w, id, data):
  text = getDialogSelection(w)
  closeDialog(w)
  print("Dialog entry %s" % id)
  print("Widget# %d" % w)
  print("Text %s" % text)

def ButtonCallback (w, id, data):
  print("Button %s" % id)
  print("Widget# %d" % w)
  print("Data.ncontrol %d" % data.ncontrol)
  print("Data.comment %s" % data.comment)
  if id == "Open":
    FileDialog("Open file", DIALOG_OPEN, "", "*", InputDialogCallback)
    return
  if id == "Save":
    FileDialog("Save file", DIALOG_SAVE, "newfile", "*", InputDialogCallback)
    return
  if id == "Exit": 
    Dialog(DIALOG_QUESTION, BUTTON_YES | BUTTON_NO, "Terminate program",\
      "Are you sure you want to close all windows "\
      "and leave the program?", ExitCallback)
    return
  if id == "Input":
    InputDialog("Data input", "Please input some data:", "like this",
      InputDialogCallback)
    return
  if id == "About...":
    Dialog(DIALOG_INFO, BUTTON_OK, "About...",
      "This is a short demo of M2Gtx features. Use at your own risk. "\
      "Copyleft Roberto Aragón, 2006-2018", None)
    return

# Create main windows and initialize toolkit 
main = MainInit("Dialog", "Demo dialog", True)

# Add a box for the buttons 
buttonbox = Box(main, HORIZONTAL, 0)

# Add an about button to the previous box 
data.append(Data(1, "Some information about this program"))
Button(buttonbox, "About...", ButtonCallback, data[len(data)-1])

# Add an input data button to the previous box 
data.append(Data(2, "Input your name in a box"))
Button(buttonbox, "Input", ButtonCallback, data[len(data)-1])

# Add an exit button to the previous box 
data.append(Data(3, "Terminates the program"))
Button(buttonbox, "Exit", ButtonCallback, data[len(data)-1])

# Add an popup menu button attached to the previous buttonbox 
popupmenu = Popup(buttonbox, "Popup menu")

# Create and add menu items to popup menu 
data.append(Data(11, "Show save dialog box"))
MenuItem(popupmenu, "Save", ButtonCallback, data[len(data)-1])

data.append(Data(12, "Show open dialog box"))
MenuItem(popupmenu, "Open", ButtonCallback, data[len(data)-1])

# Separator: callback is never called 
Separator(popupmenu)

data.append(Data(13, "Exits application"))
menuitem = MenuItem(popupmenu, "Exit", ButtonCallback, data[len(data)-1])

# Main event dispatching loop 
MainLoop()
