#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append('../../../lib/src/.libs') # remove when installed
from pygtx import MainLoop, MainInit, Label, setLabelText, Entry, getEntryText,\
                  Frame

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

notifylabel = 0
data = []

def EntryCallback(w, id, data):
  print('Text box     : %s' % id)
  print('Widget#      : %d' % w)
  text = getEntryText(w, 50)
  print('Contents     : %s' % text)
  print('Data.ncontrol: %d' % data.ncontrol)
  print('Data.comment : %s' % data.comment)
  setLabelText(notifylabel, text)

# Create main window and inicilize toolkit
main = MainInit('TextDemo', 'Demo text an entryText for Modula-2/Gtx', True)

# Add  a frame of result
frame1 = Frame(main, 'Last input', 300)

# Add an instruction label on top of the main window
notifylabel = Label(frame1, 'Please, input data and press RETURN')

# Add  a frame with a group name to the main window to gather the entries
frame2 = Frame(main, 'Personal data', 300)

# Add text entries to the previous frame
# Tab stop order is = creation order and tab group = consecutive entries
data.append(Data(101, 'One entry with max length of (9)'))
Entry(frame2, 'Simple entry: ', 9, '', EntryCallback, data[len(data) - 1])

data.append(Data(102, 'Here you can type up to (12)'))
Entry(frame2, 'Other one:    ', 12, '', EntryCallback, data[len(data) - 1])

data.append(Data(103, 'An small field for only (9)'))
Entry(frame2, 'Last:         ', 15, '', EntryCallback, data[len(data) - 1])

# Main event dispatching loop
MainLoop()
