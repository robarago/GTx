#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append('../../../lib/src/.libs') # remove when installed
from pygtx import MainLoop, MainInit, Label, setLabelText, Button, Box, Frame,\
                  HORIZONTAL

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

data = []
notifylabel = 0

def ButtonCallback(widget, id, data):
  setLabelText(notifylabel, id)
  print('Button       : %s' % id)
  print('Widget#      : %d' % widget)
  print('Data.ncontrol: %d' % data.ncontrol)
  print('Data.comment : %s' % data.comment)

# Create main windows and initialize toolkit 
main = MainInit('Button', 'Demo button', True)

# Add  a frame of result 
frame = Frame(main, 'Last clicked button', 300)

# Add an instruction label on top of the main window 
notifylabel = Label(frame, 'Please, click on a button')

# Add a box for the buttons 
buttonbox = Box(main, HORIZONTAL, 0)

# Add an about button to the previous box 
data.append(Data(1, 'Some information about this program'))
Button(buttonbox, 'About...', ButtonCallback, data[len(data) - 1])

# Add an exit button to the previous box
data.append(Data(2, 'Terminates the program'))
Button(buttonbox, 'Exit', ButtonCallback, data[len(data) - 1])
  
# Add an input data button to the previous box
data.append(Data(3, 'Input your name in a box'))
Button(buttonbox, 'Input', ButtonCallback, data[len(data) - 1])

# Main event dispatching loop 
MainLoop()
