#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append("../../../lib/src/.libs") # remove when installed
from pygtx import MainLoop, MainInit, Frame, Option, Check, isSelected, Label,\
                  setLabelText

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

notifylabel = 0
data = []

def SelectCallback (w, id, data):
  print("Option: %s" % id)
  if isSelected(w):
    setLabelText(notifylabel, "%s [Selection: ON]" % id)
    print("Selection: [ON]")
  else:
    setLabelText(notifylabel, "%s [Selection: OFF]" % id)
    print("Selection: [OFF]")
  print("Widget# %d" % w)
  print("Data.ncontrol %d" % data.ncontrol)
  print("Data.comment %s" % data.comment)

# Create main windows and initialize toolkit 
main = MainInit("Select", "Demo options and check", True)

 # Add  a frame of result 
frame1 = Frame(main, "Last option/check", 300)

# Add an instruction label on top of the main window 
notifylabel = Label(frame1, "Please, select a option or check")

# Add a frame of the options 
frame2 = Frame(main, "Please, select one option", 300)

# Add three options 
data.append(Data(201, "The one for the lazy"))
group = Option(frame2, "First option in list", False, 0, SelectCallback, data[len(data)-1])

data.append(Data(202, "An alternative"))
Option(frame2, "Second option, a chance", False, group, SelectCallback, data[len(data)-1])

data.append(Data(203, "One exclusive option"))
Option(frame2, "Third option and last", True, group, SelectCallback, data[len(data)-1])

# Add a frame of the checks 
frame3 = Frame(main, "Please, select several options", 300)

# Add three check boxes 
data.append(Data(301, "One is a number"))
Check(frame3, "One", False, SelectCallback, data[len(data)-1])

data.append(Data(302, "Two is a pair"))
Check(frame3, "Two", False, SelectCallback, data[len(data)-1])

data.append(Data(303, "Three is a crowd"))
Check(frame3, "Three", False, SelectCallback, data[len(data)-1])

# Main event dispatching loop 
MainLoop()
