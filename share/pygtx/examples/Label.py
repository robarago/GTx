#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append('../../../lib/src/.libs') # remove when installed
from pygtx import MainLoop, MainInit, Label

# Create main window and initialize toolkit
main = MainInit("Helloworld", "Demo test for Modula-2/Gtx", True)

# Add labels
Label(main, "Hello world... in Modula-2/Gtx!")
Label(main, "Hallo Welt... in Modula-2/Gtx!")
Label(main, "¡Hola mundo... desde Modula-2/Gtx!")
Label(main, "Bonjour le monde... à Modula-2/Gtx!")
Label(main, "Ciao mondo... in Modula-2/Gtx!")

# Main event dispatching loop
MainLoop()
