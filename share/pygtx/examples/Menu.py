#!/usr/bin/python
# -*- encoding: UTF-8 -*-

import sys; sys.path.append('../../../lib/src/.libs') # remove when installed
from pygtx import MainLoop, MainInit, Frame, Box, Label, setLabelText, Popup,\
                  MenuBar, Menu, MenuItem, Separator, HORIZONTAL

class Data:
  def __init__(self, n, c):
    self.ncontrol = n
    self.comment = c

data = []
notifylabel = 0

def ButtonCallback(w, id, data):
  setLabelText(notifylabel, 'Selected menu: %s' % id)
  print('Menu         : %s' % id)
  print('Widget#      : %d' % w)
  print('Data.ncontrol: %d' % data.ncontrol)
  print('Data.comment : %s' % data.comment)

# Create main windows and initialize toolkit
main = MainInit('Menu', 'Demo menu and popupmenu', True)

# Add menu bar: 0 means application top level menu bar
menubar = MenuBar(0)

# Add file menus
filemenu = Menu(menubar, 'File')

data.append(Data(1001, 'Open file dialog'))
menuitem = MenuItem(filemenu, 'Open', ButtonCallback, data[len(data)-1])

data.append(Data(1002, 'Save file dialog'))
menuitem = MenuItem(filemenu, 'Save', ButtonCallback, data[len(data)-1])

data.append(Data(1004, 'Exits application'))
menuitem = MenuItem(filemenu, 'Exit', ButtonCallback, data[len(data)-1])

# Add help menus
helpmenu = Menu(menubar, 'Help')

data.append(Data(1005, 'Show help about dialog box'))
menuitem = MenuItem(helpmenu, 'About...', ButtonCallback, data[len(data)-1])

# Add  a frame of result
frame = Frame(main, 'Last clicked button', 300)

# Add an instruction label on top of the main window
notifylabel = Label(frame, 'Please, select a menu option')

# Add a box at bottom for command buttons
buttonbox = Box(main, HORIZONTAL, 0)

# Add an popup menu button attached to the previous buttonbox
popupmenu = Popup(buttonbox, 'Popup menu')

# Create and add menu items to popup menu
data.append(Data(11, 'Show input dialog box'))
menuitem = MenuItem(popupmenu, 'Input', ButtonCallback, data[len(data)-1])

data.append(Data(12, 'Show help about dialog box'))
menuitem = MenuItem(popupmenu, 'About...', ButtonCallback, data[len(data)-1])

# Separator: callback is never called
Separator(popupmenu)

data.append(Data(13, 'Exits application'))
menuitem = MenuItem(popupmenu, 'Exit', ButtonCallback, data[len(data)-1])

# Main event dispatching loop
MainLoop()
