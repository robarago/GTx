MODULE Menu;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxContainer IMPORT Frame, Box, HORIZONTAL;
	FROM Log IMPORT DebugCard, DebugString;
	FROM GtxText IMPORT Label, setLabelText;
	FROM GtxMenu IMPORT Popup, MenuBar, Menu, MenuItem, Separator;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
  FROM SYSTEM IMPORT ADR;
  FROM StrLib IMPORT StrCopy;
  FROM Strings IMPORT Concat;

	TYPE tData = RECORD
    ncontrol: CARDINAL;
    comment: ARRAY[1..30] OF CHAR;
  END;

  VAR mwin, notifylabel, i, frame, buttonbox: CARDINAL;
	VAR menubar, popupmenu, filemenu, helpmenu, menuitem: CARDINAL;
  VAR data: ARRAY[1..7] OF tData;

  PROCEDURE ButtonCallback(
        widget: CARDINAL;
    VAR id: ARRAY OF CHAR;
    VAR data: tData);
	VAR
		msg: ARRAY[1..100] OF CHAR;
  BEGIN
		Concat("Selected menu: ", id, msg);
    setLabelText(notifylabel, msg);
    DebugString("Menu", id);
    DebugCard("Widget#", widget);
    DebugCard("Data.ncontrol", data.ncontrol);
    DebugString("Data.comment", data.comment);
  END ButtonCallback;
	
BEGIN

	(* Create main window and initialize toolkit *)
  mwin := MainInit("Menu", "Demo menu and popupmenu", TRUE, GetArgC(), GetArgV());

	(* Add menu bar: 0 means application top level menu bar *)
  menubar := MenuBar(0);

	(* Add file menus *)
  filemenu := Menu(menubar, "File");
  INC(i);
  data[i].ncontrol := 1001;
  StrCopy("Open file dialog", data[i].comment);
  menuitem := MenuItem(filemenu, "Open", ButtonCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 1002;
  StrCopy("Save file dialog", data[i].comment);
  menuitem := MenuItem(filemenu, "Save", ButtonCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 1004;
  StrCopy("Exits application", data[i].comment);
  menuitem := MenuItem(filemenu, "Exit", ButtonCallback, ADR(data[i]));

  (* Add help menus *)
  helpmenu := Menu(menubar, "Help");
  INC(i);
  data[i].ncontrol := 1005;
  StrCopy("Show help about dialog box", data[i].comment);
  menuitem := MenuItem(helpmenu, "About...", ButtonCallback, ADR(data[i]));

	(* Add  a frame of result *)
  frame := Frame(mwin, "Last clicked button", 300);

  (* Add an instruction label on top of the mwin window *)
  notifylabel := Label(frame, "Please, select a menu option");

	(* Add a box at bottom for command buttons *)
  buttonbox := Box(mwin, HORIZONTAL, 0);

	(* Add an popup menu button attached to the previous buttonbox *)
  popupmenu := Popup(buttonbox, "Popup menu");

  (* Create and add menu items to popup menu *)
  INC(i);
  data[i].ncontrol := 11;
  StrCopy("Show input dialog box", data[i].comment);
  menuitem := MenuItem(popupmenu, "Input", ButtonCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 12;
  StrCopy("Show help about dialog box", data[i].comment);
  menuitem := MenuItem(popupmenu, "About...", ButtonCallback, ADR(data[i]));
  (* Separator: callback is never called *)
  Separator(popupmenu);
  INC(i);
  data[i].ncontrol := 13;
  StrCopy("Exits application", data[i].comment);
  menuitem := MenuItem(popupmenu, "Exit", ButtonCallback, ADR(data[i]));

	(* Main event dispatching loop *)
  MainLoop;
END Menu.
