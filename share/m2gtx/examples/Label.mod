MODULE helloworld;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxText IMPORT Label;
	FROM GtxContainer IMPORT Box, VERTICAL, HORIZONTAL;

	FROM UnixArgs IMPORT GetArgC, GetArgV;

VAR
	mwin: CARDINAL;
	label: CARDINAL;
BEGIN
	(* Create main window and initialize toolkit *)
	mwin := MainInit("Helloworld", "Demo test for Modula-2/Gtx", TRUE, GetArgC(), GetArgV());

	(* Add label *)
	label := Label(mwin, "Hello world... in Modula-2/Gtx!");
	label := Label(mwin, "Hallo Welt... in Modula-2/Gtx!");
	label := Label(mwin, "¡Hola mundo... desde Modula-2/Gtx!");
	label := Label(mwin, "Bonjour le monde... à Modula-2/Gtx!");
	label := Label(mwin, "Ciao mondo... in Modula-2/Gtx!");

	(* Main event dispatching loop *)
	MainLoop;
END helloworld.
