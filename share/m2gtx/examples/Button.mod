MODULE Button;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxText IMPORT Label, setLabelText;
	FROM GtxButton IMPORT Button;
	FROM GtxContainer IMPORT Box, Frame, HORIZONTAL;
	FROM Log IMPORT DebugCard, DebugString;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
	FROM SYSTEM IMPORT ADR;
	FROM StrLib IMPORT StrCopy;

	TYPE tData = RECORD
		ncontrol: CARDINAL;
		comment: ARRAY[1..30] OF CHAR;
	END;

	VAR mwin, buttonbox, notifylabel, i, frame: CARDINAL;
	VAR data: ARRAY[1..3] OF tData;

	PROCEDURE ButtonCallback(
				widget: CARDINAL; 
		VAR id: ARRAY OF CHAR; 
		VAR data: tData);
	BEGIN
		setLabelText(notifylabel, id);
		DebugString("Button", id);
		DebugCard("Widget#", widget);
		DebugCard("Data.ncontrol", data.ncontrol);
		DebugString("Data.comment", data.comment);
	END ButtonCallback;

BEGIN
	(* Create main window and initialize toolkit *)
	mwin := MainInit("Button", "Demo button", TRUE, GetArgC(), GetArgV());

	(* Add  a frame of result *)
  frame := Frame(mwin, "Last clicked button", 300);

  (* Add an instruction label on top of the mwin window *)
  notifylabel := Label(frame, "Please, click on a button");

	(* Add a box for the buttons *)
	buttonbox := Box(mwin, HORIZONTAL);

	(* Add an about button to the previous box *)
  INC(i);
  data[i].ncontrol := 1;
  StrCopy("Some information about this program", data[i].comment);
  Button(buttonbox, "Ñaca...", ButtonCallback, ADR(data[i]));

  (* Add an input data button to the previous box *)
  INC(i);
  data[i].ncontrol := 2;
  StrCopy("Input your name in a box", data[i].comment);
  Button(buttonbox, "Input", ButtonCallback, ADR(data[i]));

  (* Add an exit button to the previous box *)
  INC(i);
  data[i].ncontrol := 3;
  StrCopy("Terminates the program", data[i].comment);
  Button(buttonbox, "Exit", ButtonCallback, ADR(data[i]));
 	
 (* Main event dispatching loop *)
  MainLoop;

END Button.
