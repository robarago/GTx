MODULE Select;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxContainer IMPORT Frame;
	FROM Log IMPORT DebugCard, DebugString;
	FROM GtxSelect IMPORT Option, Check, isSelected;
	FROM GtxText IMPORT Label, setLabelText;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
  FROM SYSTEM IMPORT ADR;
  FROM StrLib IMPORT StrCopy;
  FROM Strings IMPORT Concat;

	TYPE tData = RECORD
    ncontrol: CARDINAL;
    comment: ARRAY[1..30] OF CHAR;
  END;

  VAR mwin, notifylabel, i: CARDINAL;
	VAR frame: ARRAY[1..3] OF CARDINAL;
  VAR option1, option2, option3: CARDINAL;
  VAR check1, check2, check3: CARDINAL;
  VAR data: ARRAY[1..6] OF tData;

	PROCEDURE OptionCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR;
    VAR data: tData);
	VAR
		msg: ARRAY[1..50] OF CHAR;
  BEGIN
    DebugString("Option", id);
    IF isSelected(widget) THEN
			Concat(id, " [Selection: ON]", msg);
      DebugString("Selection: ", "[ON]");
    ELSE
			Concat(id, " [Selection: OFF]", msg);
      DebugString("Selection: ", "[OFF]");
    END;
    setLabelText(notifylabel, msg);
    DebugCard("Widget#", widget);
    DebugCard("Data.ncontrol", data.ncontrol);
    DebugString("Data.comment", data.comment);
  END OptionCallback;
	
	PROCEDURE CheckCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR;
    VAR data: tData);
	VAR
		msg: ARRAY[1..50] OF CHAR;
  BEGIN
    IF isSelected(widget) THEN
			Concat(id, " [SELECTED]", msg);
      DebugString("Selection: ", "[ON]");
      DebugString("Check id", id);
      DebugString("Selection: ", "[SELECTED]");
      DebugCard("Widget#", widget);
      DebugCard("Data.ncontrol", data.ncontrol);
      DebugString("Data.comment", data.comment);
    ELSE
			Concat(id, " [UNSELECTED]", msg);
      DebugString("Check id", id);
      DebugString("Selection : ", "[UNSELECTED]");
    END;
		setLabelText(notifylabel, msg);
  END CheckCallback;
	
BEGIN

	(* Create main window and initialize toolkit *)
  mwin := MainInit("Select", "Demo options and check", TRUE, GetArgC(), GetArgV());

  (* Add  a frame of result *)
  frame[1] := Frame(mwin, "Last option/check", 300);

  (* Add an instruction label on top of the mwin window *)
  notifylabel := Label(frame[1], "Please, select a option or check");

	(* Add a frame of the options *)
  frame[2] := Frame(mwin, "Please, select one option", 300);

	(* Add three options *)
  INC(i);
  data[i].ncontrol := 201;
  StrCopy("The one for the lazy", data[i].comment);
  option1 := Option(frame[2], "First option in list", FALSE, 0, OptionCallback, ADR(data[i]));

  INC(i);
  data[i].ncontrol := 202;
  StrCopy("An alternative", data[i].comment);
  option2 := Option(frame[2], "Second option, a chance", FALSE, option1, OptionCallback, ADR(data[i]));

  INC(i);
  data[i].ncontrol := 203;
  StrCopy("One exclusive option", data[i].comment);
  option3 := Option(frame[2], "Third option and last", TRUE, option1, OptionCallback, ADR(data[i]));

	(* Add a frame of the checks *)
  frame[3] := Frame(mwin, "Please, select several options", 300);

	(* Add three check boxes *)
  INC(i);
  data[i].ncontrol := 301;
  StrCopy("One is a number", data[i].comment);
  check1 := Check(frame[3], "One", FALSE, CheckCallback, ADR(data[i]));

  INC(i);
  data[i].ncontrol := 302;
  StrCopy("Two is a pair", data[i].comment);
  check2 := Check(frame[3], "Two", FALSE, CheckCallback, ADR(data[i]));

  INC(i);
  data[i].ncontrol := 303;
  StrCopy("Three is a crowd", data[i].comment);
  check3 := Check(frame[3], "Three", FALSE, CheckCallback, ADR(data[i]));

	(* Main event dispatching loop *)
  MainLoop;

END Select.
