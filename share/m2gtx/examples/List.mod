MODULE List;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxContainer IMPORT Frame;
	FROM Log IMPORT DebugCard, DebugString;
	FROM GtxText IMPORT Label, setLabelText;
	FROM GtxList IMPORT List, addItem, getSelectedItem, ComboList, getComboListText;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
  FROM SYSTEM IMPORT ADR;
  FROM StrLib IMPORT StrCopy, StrEqual;
	FROM Strings IMPORT Concat;

	TYPE tData = RECORD
    ncontrol: CARDINAL;
    comment: ARRAY[1..30] OF CHAR;
  END;

  VAR mwin, notifylabel, i:  CARDINAL;
	VAR frame: ARRAY[1..3] OF CARDINAL;
	VAR list, combolist: CARDINAL;
  VAR data: ARRAY[1..3] OF tData;

	PROCEDURE ListCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR;
    VAR data: tData);
  VAR
    index: CARDINAL;
    item: ARRAY [0..50] OF CHAR;
    text: ARRAY [0..50] OF CHAR;
    msg: ARRAY [0..200] OF CHAR;
  BEGIN
    getSelectedItem(widget, item, index);
    IF StrEqual(id, "combolist") THEN
      DebugString("Combo list box", id);
      getComboListText(widget, text, 50);
      DebugString("Combo list text", text);
			Concat("Combo list clicked: ", text, msg);
    ELSE
      DebugString("List box", id);
      DebugCard("Selected item index", index);
      DebugString("Selected item text", item);
			Concat("List clicked: ", item, msg);
    END;
		setLabelText(notifylabel, msg);
    (* Unselection event also notified with text NIL *)
    DebugCard("Widget#", widget);
    DebugCard("Data.ncontrol", data.ncontrol);
    DebugString("Data.comment", data.comment);
  END ListCallback;

BEGIN

	(* Create main window and initialize toolkit *)
  mwin := MainInit("List", "Demo list and combo box", TRUE, GetArgC(), GetArgV());

  (* Add  a frame of result *)
  frame[1] := Frame(mwin, "Last item selected", 400);

  (* Add an instruction label on top of the mwin window *)
  notifylabel := Label(frame[1], "Please, select an item from ths list or combo");

	(* Add frame for listbox *)
  frame[2] := Frame(mwin, "Please, click on the list", 400);

  (* Add listbox *)
  INC(i);
  data[i].ncontrol := 401;
  StrCopy("List box with simple selection", data[i].comment);
  list := List(frame[2], 1, ListCallback, ADR(data[i]));
  addItem(list, "First item with a long line");
  addItem(list, "Second item with a long long line");
  addItem(list, "Third item is a good item");
  addItem(list, "Fourth item is not a big deal");
  addItem(list, "Fifth item is easy and ready");
  addItem(list, "Sixth item and other things");
  addItem(list, "Seventh item is nearly out");
  addItem(list, "Eighth item with all nearly done");
  addItem(list, "Nineth item and last, uf!");

	(* Add frame for combobox *)
  frame[3] := Frame(mwin, "Select an item from the combobox", 400);

  (* Add combolist with prompt label *)
  INC(i);
  data[i].ncontrol := 402;
  StrCopy("Combo list box with simple selection", data[i].comment);
  combolist := ComboList(frame[3], 225, 50, ListCallback, ADR(data[i]));
  addItem(combolist, "First item of combolist");
  addItem(combolist, "Second item of combolist");
  addItem(combolist, "Third item of combolist");
  addItem(combolist, "Fourth item of combolist");

	(* Main event dispatching loop *)
  MainLoop;
END List.
