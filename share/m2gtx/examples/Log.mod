IMPLEMENTATION MODULE Log;

	FROM StrIO IMPORT WriteString, WriteLn;
	FROM NumberIO IMPORT WriteCard;
	FROM FIO IMPORT FlushOutErr;

	PROCEDURE DebugCard(msg: ARRAY OF CHAR; i: CARDINAL);
	BEGIN
		WriteString(msg);
		WriteString(" : [");
		WriteCard(i, 0);
		WriteString("]");
		WriteLn;
		FlushOutErr;
	END DebugCard;

	PROCEDURE DebugString(msg, s: ARRAY OF CHAR);
	BEGIN
		WriteString(msg);
		WriteString(" : [");
		WriteString(s);
		WriteString("]");
		WriteLn;
		FlushOutErr;
	END DebugString;

END Log.
