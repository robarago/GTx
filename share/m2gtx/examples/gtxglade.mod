IMPLEMENTATION MODULE gtxglade;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxDialog IMPORT DialogType, DialogButtons, Dialog;
	FROM GtxText IMPORT getEntryText; 

	FROM UnixArgs IMPORT GetArgC, GetArgV;
	FROM SYSTEM IMPORT ADR, ADDRESS;
	FROM StrIO IMPORT WriteString, WriteLn;
	FROM NumberIO IMPORT WriteCard;
	FROM libc IMPORT printf,exit;
	FROM StrLib IMPORT StrEqual, StrCopy;
	FROM Args IMPORT GetArg, Narg;
	FROM FIO IMPORT FlushOutErr;

	PROCEDURE EntryCallback(
				widget: CARDINAL;
		VAR id:			ARRAY OF CHAR;
				data:		ADDRESS);
	VAR
		text: ARRAY [0..50] OF CHAR;
		res: INTEGER;
	BEGIN
		WriteString("Text box     : [");
		WriteString(id);
		WriteString("]");
		WriteLn;
		WriteString("Widget#      : [");
		WriteCard(widget, 0);
		WriteString("]");
		WriteLn;
		WriteString("Contents     : [");
		getEntryText(widget, text, 50);
		WriteString(text);
		WriteString("]");
		WriteLn;
		FlushOutErr;
		(* data can't never be an ARRAY OF CHAR since it is an generic pointer *)
		res := printf("Data         : [%s]\n", data);
	END EntryCallback;

	PROCEDURE ButtonCallback(
				widget: CARDINAL;
		VAR id:			ARRAY OF CHAR;
				data:		ADDRESS);
	VAR
		res: INTEGER;
	BEGIN
		WriteString("Widget#          : [");
		WriteCard(widget, 0);
		WriteString("]");
		WriteLn;
		WriteString("Name             : [");
		WriteString(id);
		WriteString("]");
		WriteLn;
		(* data can't never be an ARRAY OF CHAR since it is an generic pointer *)
		res := printf("Data             : [%s]\n", data);
		IF StrEqual(id, "quit") THEN
			exit(0);
		ELSE
			Dialog(DIALOG_INFO, {BUTTON_YES}, "About...", "This is a short demo of M2Gtx Glade feature. Use at your own risk. Copyleft Roberto Aragón, 2006");
		END;
	END ButtonCallback;

VAR
	mwin: CARDINAL;
	classname: ARRAY [1..10] OF CHAR;
BEGIN
	IF GetArg(classname, 1) THEN
		(* Create main window and initialize toolkit *)
		mwin := MainInit(classname, "Demo test for Modula-2/Gtx with Glade", TRUE, GetArgC(), GetArgV());
		MainLoop;
	ELSE
		WriteString("Sorry: application class name / main window name (no more than 10 chars) required");
		WriteLn;
	END;
END gtxglade.
