MODULE EntryText;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxText IMPORT Label, setLabelText, Entry, getEntryText;
	FROM GtxContainer IMPORT Frame;
	FROM Log IMPORT DebugCard, DebugString;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
  FROM SYSTEM IMPORT ADR, ADDRESS;
  FROM StrLib IMPORT StrEqual, StrCopy;

	VAR mwin, notifylabel, i: CARDINAL;
	VAR data: ARRAY [1..3] OF tData;
	VAR frame: ARRAY [1..2] OF CARDINAL;

	TYPE tData = RECORD
		ncontrol: CARDINAL;
		comment: ARRAY[1..30] OF CHAR;
	END;

  PROCEDURE EntryCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR;
    VAR data: tData);
  VAR
    text: ARRAY [0..50] OF CHAR;
  BEGIN
    DebugString("Text box", id);
    DebugCard("Widget#", widget);
    getEntryText(widget, text, 50);
    DebugString("Contents", text);
    DebugCard("Data.ncontrol", data.ncontrol);
    DebugString("Data.comment", data.comment);
		setLabelText(notifylabel, text);
  END EntryCallback;

BEGIN
	(* Create main window and inicilize toolkit *)
	mwin := MainInit("TextDemo", "Demo text an entryText for Modula-2/Gtx",
									 TRUE, GetArgC(), GetArgV());

	(* Add  a frame of result *)
  frame[1] := Frame(mwin, "Last input", 300);

	(* Add an instruction label on top of the mwin window *)
	notifylabel := Label(frame[1], "Please, input data and press RETURN");

	(* Add  a frame with a group name to the mwin window to gather the entries *)
  frame[2] := Frame(mwin, "Personal data", 300);

  (* Add text entries to the previous frame *)
  (* Tab stop order is = creation order and tab group = consecutive entries *)
  INC(i);
  data[i].ncontrol := 101;
  StrCopy("One entry with max length of (30)", data[i].comment);
  Entry(frame[2], "Simple entry: ", 9, "", EntryCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 102;
  StrCopy("Here you can type up to (50)", data[i].comment);
  Entry(frame[2], "Other one:    ", 12, "", EntryCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 103;
  StrCopy("An small field for only (9)", data[i].comment);
  Entry(frame[2], "Last:         ", 15, "", EntryCallback, ADR(data[i]));

	(* Main event dispatching loop *)
	MainLoop;

END EntryText.
