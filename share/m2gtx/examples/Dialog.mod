MODULE Dialog;

	FROM Gtx IMPORT MainLoop, MainInit;
	FROM GtxContainer IMPORT Box, HORIZONTAL;
	FROM Log IMPORT DebugCard, DebugString;
	FROM GtxDialog IMPORT Dialog, InputDialog, closeDialog, DialogType,
                        DialogButtons, FileDialog, DialogMode,
												getDialogSelection;
	FROM GtxButton IMPORT Button;
	FROM GtxMenu IMPORT MenuItem, Popup, Separator;

	FROM UnixArgs IMPORT GetArgC, GetArgV;
  FROM SYSTEM IMPORT ADR;
  FROM StrLib IMPORT StrCopy, StrEqual;
	FROM libc IMPORT exit;

	TYPE tData = RECORD
    ncontrol: CARDINAL;
    comment: ARRAY[1..30] OF CHAR;
  END;

  VAR mwin, i, buttonbox: CARDINAL;
	VAR button1, button2, button3, popupmenu, menuitem: CARDINAL;
  VAR data: ARRAY[1..6] OF tData;

	PROCEDURE ExitCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR);
  BEGIN
    IF (StrEqual(id, "Yes")) THEN
      exit(0);
    ELSE
      closeDialog(widget);
    END;
  END ExitCallback;

	PROCEDURE InputDialogCallback(
        widget: CARDINAL;
    VAR id:     ARRAY OF CHAR);
  VAR
    res: CARDINAL;
		text: ARRAY[1..1024] OF CHAR;
  BEGIN
		getDialogSelection(widget, text);
    DebugString("Dialog entry", id);
    DebugCard("Widget#", widget);
    DebugString("Text", text);
		closeDialog(widget);
  END InputDialogCallback;

	PROCEDURE ButtonCallback(
				widget: CARDINAL; 
		VAR id: ARRAY OF CHAR; 
		VAR data: tData);
  BEGIN
		DebugString("Button", id);
    DebugCard("Widget#", widget);
    DebugCard("Data.ncontrol", data.ncontrol);
    DebugString("Data.comment", data.comment);

		IF StrEqual(id, "Open") THEN
      FileDialog("Open file", DIALOG_OPEN, "", "*", InputDialogCallback);
      RETURN;
    END;
    IF StrEqual(id, "Save") THEN
      FileDialog("Save file", DIALOG_SAVE, "newfile", "*", InputDialogCallback);
      RETURN;
    END;
    IF StrEqual(id, "Exit") THEN
      Dialog(DIALOG_QUESTION, {BUTTON_YES,BUTTON_NO}, "Terminate program", "Are you sure you want to close all windows and leave this program?", ExitCallback);
      RETURN;
    END;
    IF StrEqual(id, "Input") THEN
      InputDialog("Data input", "Please input some data:", "like this",
				InputDialogCallback);
      RETURN;
    END;
    IF StrEqual(id, "About...") THEN
      Dialog(DIALOG_INFO, {BUTTON_OK}, "About...", "This is a short demo of M2Gtx features. Use at your own risk. Copyleft Roberto Aragón, 2006-2018");
      RETURN;
    END;

	END ButtonCallback;	
BEGIN

	(* Create main window and initialize toolkit *)
  mwin := MainInit("Dialog", "Demo dialog", TRUE, GetArgC(), GetArgV());

  (* Add a box for the buttons *)
  buttonbox := Box(mwin, HORIZONTAL, 0);

  (* Add an about button to the previous box *)
  INC(i);
  data[i].ncontrol := 1;
  StrCopy("Some information about this program", data[i].comment);
  Button(buttonbox, "About...", ButtonCallback, ADR(data[i]));

  (* Add an input data button to the previous box *)
  INC(i);
  data[i].ncontrol := 2;
  StrCopy("Input your name in a box", data[i].comment);
  Button(buttonbox, "Input", ButtonCallback, ADR(data[i]));

  (* Add an exit button to the previous box *)
  INC(i);
  data[i].ncontrol := 3;
  StrCopy("Terminates the program", data[i].comment);
  Button(buttonbox, "Exit", ButtonCallback, ADR(data[i]));

	(* Add an popup menu button attached to the previous buttonbox *)
  popupmenu := Popup(buttonbox, "Popup menu");
	
	(* Create and add menu items to popup menu *)
  INC(i);
  data[i].ncontrol := 11;
  StrCopy("Show save dialog box", data[i].comment);
  menuitem := MenuItem(popupmenu, "Save", ButtonCallback, ADR(data[i]));
  INC(i);
  data[i].ncontrol := 12;
  StrCopy("Show open dialog box", data[i].comment);
  menuitem := MenuItem(popupmenu, "Open", ButtonCallback, ADR(data[i]));
  (* Separator: callback is never called *)
  Separator(popupmenu);
  INC(i);
  data[i].ncontrol := 13;
  StrCopy("Exits application", data[i].comment);
  menuitem := MenuItem(popupmenu, "Exit", ButtonCallback, ADR(data[i]));

	(* Main event dispatching loop *)
  MainLoop;
END Dialog.
