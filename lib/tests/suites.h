#include <check.h>

static Suite* make_Main_suite(void);
static Suite* make_Button_suite(void);
static Suite* make_Container_suite(void);
static Suite* make_Dialog_suite(void);
static Suite* make_List_suite(void);
static Suite* make_Menu_suite(void);
static Suite* make_Select_suite(void);
static Suite* make_Text_suite(void);
