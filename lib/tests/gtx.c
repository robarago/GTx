#include <check.h>
#include <stdlib.h>

#include "pygtx.h"
#include "suites.h"

#include "Main.c"
#include "Button.c"
#include "Container.c"
#include "Dialog.c"
#include "List.c"
#include "Menu.c"
#include "Select.c"
#include "Text.c"

int main(void) {
	int number_failed;
	SRunner* sr = srunner_create(make_Main_suite());

	srunner_add_suite(sr, make_Button_suite());
	srunner_add_suite(sr, make_Container_suite());
	srunner_add_suite(sr, make_Dialog_suite());
	srunner_add_suite(sr, make_List_suite());
	srunner_add_suite(sr, make_Menu_suite());
	srunner_add_suite(sr, make_Select_suite());
	srunner_add_suite(sr, make_Text_suite());
	srunner_run_all(sr, CK_VERBOSE);
	number_failed = srunner_ntests_failed(sr);
	
	return number_failed ? EXIT_FAILURE : EXIT_SUCCESS;
}

