#include <check.h>

static Suite* make_Main_suite(void) {
	Suite* s = suite_create("Main");
	TCase* tc_core = tcase_create("Core");

	//tcase_add_test(tc_core, <Main_test_case_X>);
	suite_add_tcase(s, tc_core);

	return s;
}
