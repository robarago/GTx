#include <check.h>

static Suite* make_Dialog_suite(void) {
	Suite* s = suite_create("Dialog");
	TCase* tc_core = tcase_create("Core");

	//tcase_add_test(tc_core, <Dialog_test_case_X>);
	suite_add_tcase(s, tc_core);

	return s;
}
