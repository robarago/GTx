#include <check.h>
#include <stdio.h>

#include "pygtx.h"

void FixtureCallback(unsigned int w, char* name, unsigned int lname, void* data) {
	if(data) printf("data: %s\n", (unsigned char*)data);
}

static void setup() {
	char* argv[] = { "empty" }; // gtk1 requires non-empty argv
	GtxWidgetId w = MainInit("Button", "Button fixture", 1, 1, argv);
	ck_assert_int_eq(w, 0);
}

static void teardown() {
}

START_TEST(Button_create)
{
	GtxWidgetId w = Button(0, "Button", NULL, NULL);
	ck_assert_int_eq(w, 1);
}
END_TEST

START_TEST(Button_create_withCB)
{
	GtxWidgetId w = Button(0, "Button", FixtureCallback, NULL);
	ck_assert_int_eq(w, 1);
}
END_TEST

START_TEST(Button_create_withCB_and_data)
{
	GtxWidgetId w = Button(0, "Button", FixtureCallback, (void*)"Some data");
	ck_assert_int_eq(w, 1);
}

END_TEST
static Suite* make_Button_suite(void) {
	Suite* s = suite_create("Button");
	TCase* tc_core = tcase_create("Core");

	tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_test(tc_core, Button_create);
	tcase_add_test(tc_core, Button_create_withCB);
	tcase_add_test(tc_core, Button_create_withCB_and_data);
	suite_add_tcase(s, tc_core);

	return s;
}
