#include <check.h>

static Suite* make_List_suite(void) {
	Suite* s = suite_create("List");
	TCase* tc_core = tcase_create("Core");

	//tcase_add_test(tc_core, <List_test_case_X>);
	suite_add_tcase(s, tc_core);

	return s;
}
