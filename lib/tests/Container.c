#include <check.h>

static Suite* make_Container_suite(void) {
	Suite* s = suite_create("Container");
	TCase* tc_core = tcase_create("Core");

	//tcase_add_test(tc_core, <Container_test_case_X>);
	suite_add_tcase(s, tc_core);

	return s;
}
