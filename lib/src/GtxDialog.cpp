/**
 * \file GtxDialog.cpp
 * Graphical Toolkit X for Modula-2: Dialog, InputDialog, FileDialog and accessory functions
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget*		gwidget;
extern GtxWidget		gv;
extern unsigned int	gdebug;

GTX_CONFIG_WIDGET(GtxDialogButton, released, "clicked", QPushButton, void)
GTX_CONFIG_WIDGET(GtxInputDialog, textValueSelected, "clicked", QInputDialog, const QString&)
GTX_CONFIG_WIDGET(GtxFileDialog, accepted, "clicked", QFileDialog, void)

typedef struct {
	GtxWidget dialog;
	GtxWidget selection;
	GtxWidget entry;
} GtxDialogData;

#ifdef XAW
#define pb_width 12
#define pb_height 12
static unsigned const char pb[] = {
	0x00, 0x00, 0x20, 0x00, 0x70, 0x00, 0xf8, 0x00, 0xfc, 0x01, 0xfe, 0x03,
	0x00, 0x00, 0xfe, 0x03, 0xfc, 0x01, 0xf8, 0x00, 0x70, 0x00, 0x20, 0x00};
Pixmap							pupix;
Pixmap							pixmaps[4];
typedef struct {
	Widget dirList;
	Widget fileList;
	Widget vpDirList;
	Widget vpFileList;
	Widget entry;
	Widget selection;
	Widget pathbutton;
	Widget pathpopup;
	Widget cmd[4];
	char *filter;
} GtxFileDialogWidgets;
static char fdparam[PATH_MAX]; /// For fdCallBacks
static GtxFileDialogWidgets *fdw; /// For fdCallBacks
extern "C" void fdChangeDir(GtxFileDialogWidgets *fdw, char *fulldir);
#endif

#ifdef GTK1
GdkPixmap *         pixmaps[4];
GdkBitmap *         mask[4];
#endif

/// System icons for dialog boxes
#ifdef GTK2
static const gchar *icons[] = {
	"gtk-dialog-info",
	"gtk-dialog-question",
	"gtk-dialog-warning",
	"gtk-dialog-error"
};
#elif defined(QT5)
static QMessageBox::Icon icons[] = {
	QMessageBox::Information,
	QMessageBox::Question,
	QMessageBox::Warning,
	QMessageBox::Critical
};
static QMessageBox::ButtonRole buttonRoles[] = {
	QMessageBox::YesRole,
	QMessageBox::NoRole,
	QMessageBox::AcceptRole, // Ok
	QMessageBox::RejectRole, // Cancel
	QMessageBox::AcceptRole, // Retry
	QMessageBox::AcceptRole, // Ignore
	QMessageBox::RejectRole  // Abort
};
#else
static unsigned const char icons[][GtxBitmap_width * GtxBitmap_height] = {
 {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfe,0x07,0x00,0x00,0xab,
	0x0a,0x00,0x80,0x55,0x15,0x00,0xc0,0xfa,0x2b,0x00,0x40,0xfd,0x57,0x00,0xc0,
	0xfe,0x2f,0x00,0x40,0x5d,0x57,0x00,0xc0,0xbe,0xaf,0x00,0x40,0x5d,0x57,0x01,
	0xc0,0xaa,0xaf,0x02,0x40,0xd5,0xd7,0x03,0xc0,0xea,0x2b,0x00,0x80,0xf5,0x55,
	0x00,0x00,0xeb,0x2a,0x00,0x00,0xf6,0x15,0x00,0x00,0xac,0x2a,0x00,0x00,0x54,
	0x15,0x00,0x00,0xec,0x02,0x00,0x00,0xf4,0x05,0x00,0x00,0xec,0x02,0x00,0x00,
	0x56,0x05,0x00,0x00,0xaa,0x0a,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//Info
 {
	0x00,0x00,0x00,0x00,0x00,0xf0,0x0f,0x00,0xf8,0x0c,0x30,0x07,0x04,0x03,0xc0,
	0x08,0x02,0x04,0x40,0x10,0x01,0x08,0x00,0x10,0x01,0x00,0x00,0x1c,0x11,0x00,
	0x00,0x60,0x0a,0xe0,0x01,0x40,0x04,0x30,0x03,0x80,0x02,0x30,0x03,0x80,0x02,
	0x80,0x01,0x80,0x02,0xc0,0x00,0x80,0x02,0xc0,0x00,0x80,0x02,0x00,0x00,0x40,
	0x14,0xc0,0x00,0x60,0x08,0xc0,0x00,0x1c,0x08,0x00,0x00,0x08,0x08,0x00,0x00,
	0x10,0x08,0x00,0x20,0x10,0x08,0x10,0x20,0x10,0x10,0x20,0x60,0x08,0x20,0x60,
	0x90,0x07,0x40,0x90,0x0f,0x00,0x80,0x0f,0xe0,0x00,0x00,0x00,0x10,0x01,0x00,
	0x00,0x08,0x02,0x00,0x00,0x08,0x02,0x00,0x00,0x08,0x02,0x00,0x00,0x10,0x01,
	0x00,0x00,0xe0,0x00,0x00,0x00,0x00,0x00},//Question
 {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xe0,0x00,0x00,0x00,0x10,0x01,
	0x00,0x00,0x08,0x07,0x00,0x00,0x08,0x0e,0x00,0x00,0x04,0x0e,0x00,0x00,0x04,
	0x1c,0x00,0x00,0x02,0x1c,0x00,0x00,0xe2,0x38,0x00,0x00,0xf1,0x39,0x00,0x00,
	0xf1,0x71,0x00,0x80,0xf0,0x71,0x00,0x80,0xf0,0xe1,0x00,0x40,0xf0,0xe1,0x00,
	0x40,0xf0,0xc1,0x01,0x20,0xf0,0xc1,0x01,0x20,0xf0,0x81,0x03,0x10,0xe0,0x80,
	0x03,0x10,0xe0,0x00,0x07,0x08,0xe0,0x00,0x07,0x08,0xe0,0x00,0x0e,0x04,0x00,
	0x00,0x0e,0x04,0xe0,0x00,0x1c,0x02,0xf0,0x01,0x1c,0x02,0xf0,0x01,0x38,0x01,
	0xe0,0x00,0x38,0x01,0x00,0x00,0x70,0x01,0x00,0x00,0x70,0xff,0xff,0xff,0x7f,
	0xf8,0xff,0xff,0x3f,0x00,0x00,0x00,0x00},//Excl
 {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x70,0x00,0x00,0x00,0x0c,0x00,0x00,0x60,0x02,0x00,0x00,0xf0,0x01,0x00,0xfe,
	0xd8,0x01,0x80,0xff,0xef,0x03,0xc0,0xc1,0xf7,0x03,0x70,0xf8,0xff,0x01,0x30,
	0xfe,0xff,0x00,0x18,0xff,0x7f,0x00,0x8c,0xff,0x7f,0x00,0xcc,0xff,0x7f,0x00,
	0xe6,0xff,0xff,0x00,0xe6,0xff,0xff,0x00,0xf6,0xff,0xff,0x00,0xf6,0xff,0xff,
	0x00,0xf6,0xff,0xff,0x00,0xfe,0xff,0xff,0x00,0xfe,0xff,0xff,0x00,0xfc,0xff,
	0x7f,0x00,0xfc,0xff,0x7f,0x00,0xf8,0xff,0x3f,0x00,0xf0,0xff,0x1f,0x00,0xf0,
	0xff,0x1f,0x00,0xc0,0xff,0x07,0x00,0x80,0xff,0x03,0x00,0x00,0xfe,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}//Error
};
extern "C" GtxReturn* getReturnData(GtxWidget w);
#endif
static const char *buttons[] = {
	"Yes",
	"No",
	"OK",
	"Cancel",
	"Retry",
	"Ignore",
	"Abort"
};

extern "C" {

GtxWidgetId
Dialog(
	int type,
	unsigned long buttonconf,
	char *title,
	char *msg,
	GtxEvent fn)
{
	GtxWidget d;
	GtxWidget b;
	GtxReturn *ret;
	int i;

#ifdef XAW
	int j;
	// Wordwrapping at next space following Nth column
	GTX_TO_LATIN1(msg)
	for(i = 0, j = 0; i<strlen((const char*)l1_msg); i++, j++)
		if(j > GTX_LABEL_WORDWRAP_COLUMN && l1_msg[i] == ' ') {
			l1_msg[i] = 10;
			j = 0;
		}
	if(!pixmaps[type])
		pixmaps[type] = XCreateBitmapFromData(XtDisplay(gv),
			RootWindowOfScreen(XtScreen(gv)), (const char*)icons[type], GtxBitmap_width,
			GtxBitmap_height);
	d = XtVaCreatePopupShell(title, transientShellWidgetClass, gv, NULL);
	Widget dialog = XtVaCreateManagedWidget("dialog", dialogWidgetClass, d,
																					XtNlabel, l1_msg,
																					XtNicon, pixmaps[type], NULL);
	XtPopdownIDRec *pr = GTX_TYPE_MALLOC(XtPopdownIDRec);
	memset(pr, 0, sizeof(XtPopdownIDRec));
	pr->shell_widget = d;
#elif defined(QT5)
	QMessageBox* mb = new QMessageBox;
	mb->setWindowTitle(QString::fromUtf8(title));
	mb->setText(QString::fromUtf8(msg));
	mb->setIcon(icons[type]);
	d = mb;
#else
	GtkWidget *m, *c, *im;

	// Dialog window creation
	d = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(d), title);
	gtk_window_set_modal(GTK_WINDOW(d), TRUE);

	// Frame containing icon and message
	#ifdef GTK3
	c = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	#else
	c = gtk_hbox_new(FALSE, 0);
	#endif
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->vbox), c, TRUE, TRUE, 10);

	// Icon, first in the main frame
#ifdef GTK2
	im = gtk_image_new_from_stock(icons[type], GTK_ICON_SIZE_DIALOG);
	gtk_widget_set_size_request(im, 48, 0);
	gtk_box_pack_start(GTK_BOX(c), im, FALSE, TRUE, 10);
	m = gtk_label_new(msg);
	gtk_label_set_line_wrap(GTK_LABEL(m), TRUE);
#else
	// Lazy creation of pixmaps for two reasons: GTK is lazy (in creating gv->window) so we must too
	if(pixmaps[type] == NULL) {
		GtkStyle *style = gtk_widget_get_style(gv);
		mask[type] = gdk_bitmap_create_from_data(gv->window, (const gchar*)icons[type],
																						 GtxBitmap_width, GtxBitmap_height);
		pixmaps[type] = gdk_pixmap_create_from_data(gv->window, (const gchar*)icons[type],
																						    GtxBitmap_width, GtxBitmap_height,
																						    -1, &style->white, &style->black);
	}
	im = gtk_pixmap_new(pixmaps[type], mask[type]);
	gtk_box_pack_start(GTK_BOX(c), im, FALSE, TRUE, 10);
	if(msg != NULL) {
		GTX_TO_LATIN1(msg);
		//m = gtk_label_new((const gchar*)l1_msg); // Doesn't work with latin1 nor UTF-8
		m = gtk_frame_new((const gchar*)l1_msg);
		gtk_frame_set_shadow_type(GTK_FRAME(m), GTK_SHADOW_NONE);
	}
#endif
	// Mensaje: segundo en el marco horizontal
	gtk_box_pack_start(GTK_BOX(c), m, FALSE, FALSE, 10);
#endif
	unsigned int did = regWidget(d);
	// If return function not available, the only button feasible is OK
	if(!fn) {
#ifdef XAW
		XawDialogAddButton(dialog, buttons[2], XtCallbackPopdown, pr);
#elif defined(QT5)
		mb->addButton(QMessageBox::Ok);
#else
		b = gtk_button_new_with_label(buttons[2]);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area), b, TRUE, TRUE, 0);
		gtx_sig_conn_obj(b, "clicked", gtk_widget_destroy, d);
#endif
	}
	else {
		// Create and connect button events
		for(i = 0; i < 7; i++) {
			if(buttonconf & (1 << i)) {
#ifdef XAW
				b = XtCreateManagedWidget(buttons[i], commandWidgetClass, dialog, NULL, 0);
#elif defined(QT5)
				b = new GtxDialogButton;
				((QPushButton*)b)->setText(buttons[i]);
				mb->addButton((QAbstractButton*)b, buttonRoles[i]);
#else
				b = gtk_button_new_with_label(buttons[i]);
				gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area), b, TRUE, TRUE, 0);
#endif
				ret = createReturn(fn, (char*)buttons[i], b, NULL, NULL, NULL);
				ret->id = did;
				gtx_sig_conn(b, GtxDialogButton, superCallback, ret);
			}
		}
	}
	// Show dialog
#ifdef XAW
	XtPopup(d, XtGrabExclusive);
	protect_close(d);
#elif defined(QT5)
	mb->show();
#else
	gtk_widget_show_all(d);
#endif

	return did;
}

GtxWidgetId
InputDialog(
	char *title,
	char *msg,
	char* text,
	GtxEvent fn)
{
	GtxWidget t; /// Text entry widget
	GtxWidget d; /// Dialog widget
	GtxWidget b; /// Dialog button widget
	GtxReturn *ret;
#ifdef XAW
	d = XtVaCreatePopupShell(title, transientShellWidgetClass, gv, NULL);
	Widget dialog = XtVaCreateManagedWidget("inputDialog", dialogWidgetClass, d,
																					XtNlabel, msg, XtNvalue, text, NULL);
	XtPopdownIDRec *pr = GTX_TYPE_MALLOC(XtPopdownIDRec);
	memset(pr, 0, sizeof(XtPopdownIDRec));
	pr->shell_widget = d;
	t = XtNameToWidget(dialog, "value");
  XtOverrideTranslations(t, XtParseTranslationTable("<Key>Return: no-op()\n"));
	XawDialogAddButton(dialog, buttons[3], XtCallbackPopdown, pr); // Cancel
	b = XtCreateManagedWidget(buttons[2], commandWidgetClass, dialog, NULL, 0);
	XtPopup(d, XtGrabExclusive);
	protect_close(d);
#elif defined(QT5)
	b = t = d = new GtxInputDialog;
	QInputDialog* id = (QInputDialog*)d;
	id->setInputMode(QInputDialog::TextInput);
	id->setWindowTitle(QString::fromUtf8(title));
	id->setLabelText(QString::fromUtf8(msg));
	id->setTextValue(QString::fromUtf8(text));
	id->show();
#else
	d = gtk_dialog_new();
	gtk_window_set_modal(GTK_WINDOW(d), TRUE);
	gtk_container_set_border_width(GTK_CONTAINER(GTK_DIALOG(d)->vbox), 10);
	// Message: second in horizontal order
	GtkWidget *m = gtk_label_new(msg);
	gtk_misc_set_alignment(GTK_MISC(m), 0.0, 0.0);
	gtk_label_set_justify(GTK_LABEL(m), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->vbox), m, TRUE, TRUE, 0);
	// Text entry
	t = gtk_entry_new_with_max_length(PATH_MAX);
	gtk_entry_set_text(GTK_ENTRY(t), text);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->vbox), t, TRUE, TRUE, 0);
	// Cancel button
	GtkWidget *cancel = gtk_button_new_with_label(buttons[3]);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area), cancel, TRUE, TRUE,0);
	gtx_sig_conn_obj(cancel, "clicked", gtk_widget_destroy, d); // Cancel
	// Ok button
	b = gtk_button_new_with_label(buttons[2]);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area), b, TRUE, TRUE, 0);
	gtk_widget_show_all(d);
#endif
	GtxDialogData* ddata = GTX_TYPE_MALLOC(GtxDialogData);
	ddata->dialog = d;
	ddata->entry = t;
	ret = createReturn(fn, (char*)"inputDialog", b, NULL, ddata, NULL);
	// Connect callback
	gtx_sig_conn(b, GtxInputDialog, superCallback, ret);

	return ret->id;
}

void
getDialogSelection(
	int b,      ///< source widget generation event
	char* text) ///< array of char with selected or typed text (or NULL)
{
	GtxWidget w = gwidget[b];
#ifdef XAW
	GtxReturn* ret = getReturnData(w); // TODO: ensure NN
	GtxDialogData* dd = (GtxDialogData*)ret->pdata;
	if(XtClass(dd->dialog) == transientShellWidgetClass) {
		char* l1_text = XawDialogGetValueString(XtParent(dd->entry));
		latin1_to_utf8((unsigned char*)l1_text, (unsigned char*)text);
	}
	else {
		Widget selection = dd->selection;
		Widget entry = dd->entry;
		char* aux1 = NULL;
		char* aux2 = NULL;
		XtVaGetValues(selection, XtNlabel, &aux1, NULL);
		GTX_DBG("Path: %s\n", aux1);
		XtVaGetValues(entry, XtNstring, &aux2, NULL);
		GTX_DBG("File: %s\n", aux2);
		strcpy(text, aux1);
		strcat(text, "/");
		strcat(text, aux2);
	}
#elif defined(QT5)
	QInputDialog* id = qobject_cast<QInputDialog*>(w);
	if(NULL != id) {
		char* p = id->textValue().toUtf8().data();
		strcpy(text, p);
	}
	else { // QFileDialog
		QFileDialog* fd = (QFileDialog*)w;
		QStringList fileNames = fd->selectedFiles();
		if(fileNames.size() > 0) {
			char* p = fileNames.first().toUtf8().data();
			strcpy(text, p);
		}
	}
#else
	GtxReturn* ret = getReturnData(w); // TODO: ensure NN
	GtxDialogData* dd = (GtxDialogData*)ret->pdata;
	if(GTK_IS_FILE_SELECTION(GTK_OBJECT(dd->dialog)))
		strcpy((char*)text, (const char*)gtk_file_selection_get_filename(GTK_FILE_SELECTION(dd->dialog)));
	else {
		strcpy((char*)text, (const char*)gtk_entry_get_text(GTK_ENTRY(dd->entry)));
	#ifdef GTK1
		GTX_TO_UTF8(text)
		strcpy((char*)text, (const char*)u8_text);
	#endif
	}
#endif
}

void
closeDialog(int w) {
#ifdef XAW
	Widget d = gwidget[w];
	while(d) {
		if(XtClass(d) == transientShellWidgetClass) {
			XtPopdown(d);
			break;
		}
		else d = XtParent(d);
	}
#elif defined(QT5)
	QDialog* d = (QDialog*)gwidget[w];
	if(d->isVisible()) d->close();
#else
	GtkWidget* d = gwidget[w];
	while(d) {
		if(GTK_IS_DIALOG(GTK_OBJECT(d)) || GTK_IS_FILE_SELECTION(GTK_OBJECT(d))) {
			gtk_widget_destroy(d);
			break;
		}
		else d = d->parent;
	}
#endif
}

#ifdef XAW
void
getSepDirEntries(
  char *path,
  char *pattern,
  char ***dirs,
	unsigned int *ndirs,
  char ***files,
	unsigned int *nfiles)
{
  struct dirent **namelist;
  int             n, i;
	char						*p;
	int             isdir;
	char						buf[PATH_MAX];

	*ndirs = *nfiles = 0;
  n = scandir(path, &namelist, 0, alphasort);
	for(i = 0; i < n; i++) {
		if(namelist[i]->d_type == DT_LNK) {
			struct stat stbuf;
			sprintf(buf, "%s/%s", path, namelist[i]->d_name);
			isdir = stat(buf, &stbuf) < 0 ? 0 : S_ISDIR(stbuf.st_mode);
		}
		else isdir = namelist[i]->d_type == DT_DIR &&
			(namelist[i]->d_name[0] != '.' || !strcmp(namelist[i]->d_name, ".."));
		if(isdir) {
			*dirs = (char **)realloc(*dirs, ++*ndirs * sizeof(char *));
			// One more char for "/"
			(*dirs)[*ndirs - 1] = GTX_STR_MALLOC(strlen(namelist[i]->d_name)
				+ sizeof(char));
			strcpy((*dirs)[*ndirs - 1], namelist[i]->d_name);
			strcat((*dirs)[*ndirs - 1], "/");
		}
		else {
			p = GTX_STR_MALLOC(strlen(pattern));
			strcpy(p, pattern);
			p = strtok(p, ";");
			while(p) {
				if(0 == fnmatch(p, namelist[i]->d_name, FNM_PATHNAME | FNM_PERIOD)) {
					*files = (char **)realloc(*files, ++*nfiles * sizeof(char *));
					(*files)[*nfiles - 1] = GTX_STR_MALLOC(strlen(namelist[i]->d_name));
					strcpy((*files)[*nfiles - 1], namelist[i]->d_name);
				}
				p = strtok(NULL, ";");
			}
		}
		free(namelist[i]);
	}
}

void
fdDirRefresh() {
	char *dir = NULL;
	XtVaGetValues(fdw->selection, XtNlabel, &dir, NULL);
	fdChangeDir(fdw, dir);
}

void
fdDirChangeCallback(Widget w, XtPointer clientData, XtPointer callData)
{
	char *fulldir = NULL;
	char *newdir = NULL;
	char *dir = NULL;

	GtxFileDialogWidgets *fdw = (GtxFileDialogWidgets *)clientData;
	if(XtClass(w) == listWidgetClass) {
		XawListReturnStruct *lrs = XawListShowCurrent(w);
		XtVaGetValues(fdw->selection, XtNlabel, &dir, NULL);
		/// Use full path plus directory selection
		newdir = GTX_STR_MALLOC(strlen(dir) + 1 + strlen(lrs->string));
		strcpy(newdir, dir);
		strcat(newdir, "/");
		strcat(newdir, lrs->string);
		fulldir = GTX_STR_MALLOC(PATH_MAX); // Non-local storage
		memset(fulldir, 0, PATH_MAX + 1);
		fulldir = realpath(newdir, fulldir);
	}
	else { /// Menu item: label is fulldir
		fulldir = GTX_STR_MALLOC(PATH_MAX); // Non-local storage
		XtVaGetValues(w, XtNlabel, &dir, NULL);
		strcpy(fulldir, dir);
	}
	/// Update all widgets for new dir location
	fdChangeDir(fdw, fulldir);
}

void
fdChangeDir(GtxFileDialogWidgets *fdw, char *fulldir)
{
	char ndir[32]; // Maybe only 25 or 26 chars long
	char *newdir = NULL;
	static char **dirs = NULL;
	static char **files = NULL;
	unsigned int ndirs = 0;
	unsigned int nfiles = 0;

	/// Destroy previous popup
	XtDestroyWidget(fdw->pathpopup);
	/// New popup
	Widget mp = XtCreatePopupShell("menu", simpleMenuWidgetClass,
																fdw->pathbutton, NULL, 0);
	fdw->pathpopup = mp;
	/// Create popup menu items
	char *p = strdup(fulldir);
	for( ;; ) {
		XtAddCallback(XtCreateManagedWidget(p, smeBSBObjectClass, mp, NULL, 0),
									XtNcallback, fdDirChangeCallback, fdw);
		if(!strcmp(p, "/")) break;
		p = dirname(p);
	}
	/// Set new dir at Selection: label with pretty printing
	newdir = strdup(fulldir);
	XtVaSetValues(fdw->selection, XtNlabel, newdir, NULL);
	if(strlen(newdir) > 24) {
		strncpy(ndir, newdir, 12);
		ndir[12] = '\0';
		strcat(ndir, "...");
		strcat(ndir, newdir + strlen(newdir) - 8);
	}
	else strcpy(ndir, newdir);
	XtVaSetValues(fdw->pathbutton, XtNlabel, &ndir, NULL);
	/// Get new dir & files lists contents
	getSepDirEntries(	newdir, fdw->filter ? fdw->filter : (char*)"*",
										&dirs, &ndirs, &files, &nfiles);
	/// Set new dir & files lists contents
	XawListChange(fdw->dirList, (const char**)dirs, ndirs, -1, True);
	if(nfiles) XawListChange(fdw->fileList, (const char**)files, nfiles, -1, True);
	else XtVaSetValues(fdw->fileList, XtNlist, NULL, NULL);
	/// Resize dir & files viewport to fixed size for optimal viewing
	XtVaSetValues(fdw->vpDirList, XtNheight, 150, XtNwidth, 191, NULL);
	XtVaSetValues(fdw->vpFileList, XtNheight, 150, XtNwidth, 191, NULL);
}

void
fdFileListCallback(Widget w, XtPointer clientData, XtPointer callData)
{
	/// Pass selection to entry text box
	GtxFileDialogWidgets *fdw = (GtxFileDialogWidgets *)clientData;
	XawListReturnStruct *lrs = XawListShowCurrent(w);
	XtVaSetValues(fdw->entry, XtNstring, lrs->string, NULL);
}

void
fdCmdDeleteCallback(unsigned int widget, char* id)
{
	if(!strcmp(id, "Yes")) {
		GTX_DBG("Deleting file '%s'...\n", fdparam);
		if(!unlink(fdparam)) {
			GTX_DBG("File deleted.\n");
			fdDirRefresh();
		} else perror("unlink");
	}
	else printf("File deletion aborted.\n");
	closeDialog(widget);
}

void
fdCmdMkDirCallback(unsigned int widget, char* id, unsigned int nid,
									 char* data)
{
	char dir[PATH_MAX], dirname[PATH_MAX];
	GTX_DBG("Widget# %d ID %s Param %s\n", widget, id, fdparam);
	getDialogSelection(widget, dirname);
	closeDialog(widget);
	strcpy(dir, fdparam);
	strcat(dir, dirname);
	GTX_DBG("Creating directory '%s'...\n", dir);
	if(!mkdir(dir, 0755)) {
		GTX_DBG("Directory created.\n");
		fdDirRefresh();
	}
	else perror("mkdir");
}

void
fdCmdRenameCallback(unsigned int widget, char* id, unsigned int nid,
										char* data)
{
	char newpath[PATH_MAX], newname[PATH_MAX];
	char* oldpath = fdparam;
	char* p = NULL;

	getDialogSelection(widget, newname);
	closeDialog(widget);
	GTX_DBG("Widget# %d ID %s newname: %s oldpath: %s\n", widget, id, newname,
		oldpath);
	strcpy(newpath, oldpath);
	p = 1 + strrchr(newpath, '/'); // start of filename
	GTX_DBG("Renaming '%s' to '%s'\n", p, newname);
	strcpy(p, newname); // copy newname past old path
	if(!rename(oldpath, newpath)) {
		GTX_DBG("File renamed.\n");
		fdDirRefresh();
	}
	else perror("rename");
}

void
fdCmdCallback(Widget w, XtPointer clientData, XtPointer callData)
{
	int i = -1;
	char *p = NULL;
	XawListReturnStruct *lrs = NULL;
	//GtxFileDialogWidgets *fdw = (GtxFileDialogWidgets *)clientData;
	fdw = (GtxFileDialogWidgets *)clientData;

	while(w != fdw->cmd[++i]);
	switch(i) {
		case 0: // Home
			fdChangeDir(fdw, getenv("HOME"));
			break;
		case 1: // Make dir
			XtVaGetValues(fdw->selection, XtNlabel, &p, NULL);
			strcpy(fdparam, (const char*)p);
			strcat(fdparam, (const char*)"/");
			InputDialog((char*)"Make directory",
				(char*)"Please enter new directory name:", (char*)"New directory",
				(GtxEvent)fdCmdMkDirCallback);
			break;
		case 2: // Delete file
			lrs = XawListShowCurrent(fdw->fileList);
			if(lrs && lrs->string && lrs->string[0]) {
				char prompt[PATH_MAX];
				XtVaGetValues(fdw->selection, XtNlabel, &p, NULL);
				strcpy(fdparam, (const char*)p);
				strcat(fdparam, (const char*)"/");
				strcat(fdparam, (const char*)lrs->string);
				sprintf(prompt, "Really delete file \"%s\"?", lrs->string);
				/// DIALOG_QUESTION with BUTTON_YES and BUTTON_NO
				Dialog(1, 3, (char*)"Delete file", prompt,
					(GtxEvent)fdCmdDeleteCallback);
			}
			break;
		case 3: // Rename file
			lrs = XawListShowCurrent(fdw->fileList);
			if(lrs && lrs->string && lrs->string[0]) {
				XtVaGetValues(fdw->selection, XtNlabel, &p, NULL);
				strcpy(fdparam, (const char*)p);
				strcat(fdparam, (const char*)"/");
				strcat(fdparam, (const char*)lrs->string);
				InputDialog((char*)"Rename file", (char*)"Please enter new file name:",
										(char*)lrs->string, (GtxEvent)fdCmdRenameCallback);
			}
			break;
	}
}
#endif

GtxWidgetId
FileDialog(
	char *title,
	int mode,
	char *filename,
	char* filter,
	GtxEvent fn)
{
	GtxWidget f;
	GtxWidget e;
	GtxWidget ok;
	GtxWidget cancel;
	GtxReturn *ret;
#ifdef XAW
	//GtxFileDialogWidgets *fdw = NULL;
	char fullpath[PATH_MAX];
	char fulldir[PATH_MAX];
	char *label = NULL;
	char **dirs = NULL;
	char **files = NULL;
	const char *commands[] = { "Home", "Make Dir", "Delete", "Rename" };
	unsigned int ndirs = 0;
	unsigned int nfiles = 0;
	int i = 0;

	char *dncopy = GTX_STR_MALLOC(strlen(filename));
	strcpy(dncopy, filename);
	char *bncopy = GTX_STR_MALLOC(strlen(filename));
	strcpy(bncopy, filename);
	char *dir = dirname(dncopy);
	char *file = basename(bncopy);

	if(dir[0] == '.') {
		if(0 != getcwd(dir, 0)) {
			perror("getcwd");
			return -1;
		}
		if(NULL == realpath(dir, fulldir)) {
			perror("realpath");
			return -1;
		}
		dir = fulldir;
		strcpy(fullpath, fulldir);
		strcat(fullpath, "/");
		strcat(fullpath, file);
	}
	else strcpy(fullpath, filename);
	fdw = GTX_TYPE_MALLOC(GtxFileDialogWidgets);
	Widget d = XtVaCreatePopupShell(title, transientShellWidgetClass, gv, NULL);
	/// Vbox for all widgets
	Widget v = XtVaCreateManagedWidget(	"fdForm", formWidgetClass, d,
		                              		XtNborderWidth, 0, NULL);
	/// Top hForm to hold buttons and path popup
	Widget bb = XtVaCreateManagedWidget("fdPathPopup", boxWidgetClass, v,
		                              		XtNborderWidth, 0, XtNorientation, 0,
																			NULL);
	/// Path popup menu
	if(!pupix) pupix = XCreateBitmapFromData(XtDisplay(gv),
                                        	RootWindowOfScreen(XtScreen(gv)),
                                        	(const char*)pb, pb_width, pb_height);
	Widget mb = XtVaCreateManagedWidget(fulldir, menuButtonWidgetClass, bb,
			                              XtNleftBitmap, pupix, NULL);
	Widget mp = XtCreatePopupShell("menu", simpleMenuWidgetClass, mb, NULL, 0);
	/// HBox for sitting lists side to side 
	Widget h = XtVaCreateManagedWidget(	"fdHForm", formWidgetClass, v,
																			XtNfromVert, bb, XtNborderWidth, 0, NULL);
	/// Dir list box
	Widget ld = XtVaCreateManagedWidget("Directories", labelWidgetClass, h,
																			XtNborderWidth, 0, XtNjustify,
																			XtJustifyLeft, NULL);
  Widget vd = XtVaCreateManagedWidget("fdViewDir", viewportWidgetClass, h,
                                     	XtNallowVert, True,
                                     	XtNallowHoriz, True,
																			XtNheight, 150,
																			XtNwidth, 191,
																			XtNresizable, True,
																			XtNforceBars, False,
																			XtNuseBottom, True,
																			XtNuseRight, True,
																			XtNfromVert, ld,
																			NULL);
  Widget dl = XtVaCreateManagedWidget(NULL, listWidgetClass, vd,
																			XtNdefaultColumns, 1,
																			XtNforceColumns, 1,
																			XtNheight, 150,
																			XtNwidth, 191,
																			NULL);
	/// File list box
	Widget lf = XtVaCreateManagedWidget("Files", labelWidgetClass, h,
																			XtNborderWidth, 0, XtNjustify,
																			XtJustifyLeft, XtNfromHoriz, vd, NULL);
  Widget vf = XtVaCreateManagedWidget("fdViewFile", viewportWidgetClass, h,
                                     	XtNallowVert, True,
                                     	XtNallowHoriz, True,
																			XtNheight, 150,
																			XtNwidth, 191,
																			XtNresizable, True,
																			XtNforceBars, False,
																			XtNuseBottom, True,
																			XtNuseRight, True,
																			XtNfromVert, lf,
																			XtNfromHoriz, vd,
																			NULL);
  Widget fl = XtVaCreateManagedWidget(NULL, listWidgetClass, vf,
																			XtNdefaultColumns, 1,
																			XtNforceColumns, 1,
																			XtNheight, 150,
																			XtNwidth, 191,
																			NULL);
	/// Fill list boxes
	getSepDirEntries(dir, filter?filter:(char*)"*", &dirs, &ndirs, &files, &nfiles);
	XawListChange(dl, (const char**)dirs, ndirs, -1, True);
	XawListChange(fl, (const char**)files, nfiles, -1, True);
	/// Operations commands
	Widget cc = XtVaCreateManagedWidget("fdBBox", boxWidgetClass, v,
		                              		XtNborderWidth, 0, XtNfromVert, h,
																			XtNorientation, 0, NULL);
	for(i = 0; i < 4; i++)
		fdw->cmd[i] = XtVaCreateManagedWidget(commands[i], commandWidgetClass, cc,
																					NULL);
	/// Selection prompt & label
	Widget l = XtVaCreateManagedWidget(	"fdLForm", formWidgetClass, v,
																			XtNborderWidth, 0,
																			XtNfromVert, cc, NULL);
	Widget lp = XtVaCreateManagedWidget("Selection:", labelWidgetClass, l,
																			XtNborderWidth, 0, XtNjustify,
																			XtJustifyLeft, NULL);
	f = XtVaCreateManagedWidget(fulldir, labelWidgetClass, l,
															XtNborderWidth, 0, XtNjustify,
															XtJustifyLeft, XtNfromHoriz, lp,
															XtNwidth, 310, NULL);
	/// File entry text box
	Widget t = XtVaCreateManagedWidget(	"fdEBox", boxWidgetClass, v,
		                              		XtNborderWidth, 0, XtNfromVert, l, NULL);
	e = XtVaCreateManagedWidget("value", asciiTextWidgetClass, t,
															XtNstring, file, XtNwidth, 388, XtNeditType,
															XawtextEdit, XtNlength, PATH_MAX, NULL);
  XtOverrideTranslations(e, XtParseTranslationTable("<Key>Return: no-op()\n"
													"<Btn1Down>: set-keyboard-focus() select-start()\n"));
	/// Buttons in a box
	Widget b = XtVaCreateManagedWidget(	"fdBBox", boxWidgetClass, v,
		                              		XtNborderWidth, 0, XtNorientation, 0,
																			XtNfromVert, t, XtNvSpace, 0, NULL);
	XtPopdownIDRec *pr = GTX_TYPE_MALLOC(XtPopdownIDRec);
	memset(pr, 0, sizeof(XtPopdownIDRec));
	pr->shell_widget = d;
	ok = XtCreateManagedWidget(buttons[2], commandWidgetClass, b, NULL, 0);
	cancel = XtCreateManagedWidget(buttons[3], commandWidgetClass, b, NULL, 0);
	fdw->dirList = dl;
	fdw->vpDirList = vd;
	fdw->fileList = fl;
	fdw->vpFileList = vf;
	fdw->entry = e;
	fdw->selection = f;
	fdw->filter = filter;
	fdw->pathbutton = mb;
	fdw->pathpopup = mp;
	/// Callbacks
	XtAddCallback(cancel, XtNcallback, XtCallbackPopdown, pr);
	XtAddCallback(dl, XtNcallback, fdDirChangeCallback, fdw);
	XtAddCallback(fl, XtNcallback, fdFileListCallback, fdw);
	for(i = 0; i < 4; i++)
		XtAddCallback(fdw->cmd[i], XtNcallback, fdCmdCallback, fdw);
	/// Fill path popup with menu items and assign callback
	char *p = strdup(fullpath);
	do {
		p = dirname(p);
		XtAddCallback(XtCreateManagedWidget(p, smeBSBObjectClass, mp, NULL, 0),
									XtNcallback, fdDirChangeCallback, fdw);
  } while(strcmp(p, "/"));
	/// Popup dialog
	XtPopup(d, XtGrabExclusive);
	protect_close(d);
#elif defined(QT5)
	ok = e = f = new GtxFileDialog;
	QFileDialog* fd = (QFileDialog*)f;
	fd->setWindowTitle(QString::fromUtf8(title));
	fd->setFileMode(QFileDialog::AnyFile);
	fd->setAcceptMode(mode?QFileDialog::AcceptSave:QFileDialog::AcceptOpen);
	fd->setNameFilter(QString::fromUtf8(filter));
	fd->selectFile(QString::fromUtf8(filename));
	fd->show();
#else
	f = gtk_file_selection_new(title);
	if(NULL != filter) gtk_file_selection_complete(GTK_FILE_SELECTION(f), filter);
	gtk_file_selection_set_filename(GTK_FILE_SELECTION(f), filename);
	e = GTK_FILE_SELECTION(f)->selection_entry;
	ok = GTK_FILE_SELECTION(f)->ok_button;
	cancel = GTK_FILE_SELECTION(f)->cancel_button;
	gtk_widget_show(f);
	gtx_sig_conn_obj(cancel, "clicked", gtk_widget_destroy, f);
#endif
	GtxDialogData* ddata = GTX_TYPE_MALLOC(GtxDialogData);
	ddata->entry = e;
	ddata->selection = f;
	ddata->dialog = f;
	ret = createReturn(fn, (char*)"fileDialog", ok, NULL, ddata, NULL);
	gtx_sig_conn(ok, GtxFileDialog, superCallback, ret);

	return ret->id;
}

} // extern "C"
