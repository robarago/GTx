/**
 * \file GtxContainer.cpp
 * Graphical Toolkit X for Modula-2: Frame, Box, Canvas, Separator
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget*	gwidget;

extern "C" {

/**
 * Generic box creation function
 * No need to return GtxWidgetId, since it has no callback
 */
unsigned int
Box(int parent, ///< widget id of box parent
	int orientation, ///< widget orientation within box: 0-horizontal, 1-vertical
	int innerspace ///< border around widgets
	)
{
	GtxWidget b;

#ifdef XAW
	b = XtVaCreateManagedWidget("box", boxWidgetClass, gwidget[parent], XtNsensitive, True,
		XtNborderWidth, 0, XtNwidth, orientation < 1 ? 0 : 1000, XtNheight,
		orientation > 0 ? 0 : 1000, XtNhorizDistance, innerspace, XtNvertDistance, innerspace, NULL);
#elif defined(QT5)
	b = new QWidget;
	b->setLayout(orientation == 0 ? (QBoxLayout *)new QVBoxLayout : (QBoxLayout *)new QHBoxLayout);
	b->layout()->setSpacing(innerspace);
	gwidget[parent]->layout()->addWidget(b);
#else
	#ifdef GTK3
	GtxWidget m = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	#else
	GtxWidget m = gtk_hbox_new(FALSE, 0);
	#endif
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), m, TRUE, TRUE, 10);
	#ifdef GTK3
	b = gtk_button_box_new(orientation ? GTK_ORIENTATION_HORIZONTAL : GTK_ORIENTATION_VERTICAL);
	#else
	b = orientation?gtk_hbutton_box_new():gtk_vbutton_box_new();
	#endif
	gtk_container_set_border_width(GTK_CONTAINER(b), 5);
	gtk_box_pack_start(GTK_BOX(m), b, TRUE, TRUE, 10);
	#ifdef GTK3
	gtk_box_set_spacing(GTK_BOX(m), innerspace);
	#else
	gtk_button_box_set_spacing(GTK_BUTTON_BOX(b), innerspace);
	#endif
#endif

	return regWidget(b);
}

/**
 * Frame creation function
 * No need to return GtxWidgetId, since it has no callback
 */
unsigned int
Frame(
	int parent, ///< widget id of box parent
	char *title, ///< title of the frame
	unsigned int width) ///< width of the frame (only used by Xaw)
{
	GtxWidget m;
	GtxWidget c;

#ifdef XAW
	GTX_TO_LATIN1(title);
	c = XtVaCreateManagedWidget((const char*)l1_title, boxWidgetClass, gwidget[parent],
															XtNborderWidth, 1, NULL);
	m = XtVaCreateManagedWidget((const char*)l1_title, labelWidgetClass, c,
															XtNborderWidth, 0, XtNwidth, width,
															XtNjustify, XtJustifyLeft, NULL);
#elif defined(QT5)
	QGroupBox *g = new QGroupBox(title);
	g->setLayout(new QVBoxLayout);
	g->setMinimumWidth(width);
	gwidget[parent]->layout()->addWidget(g);
	c = (QWidget *)g;
#else
	#ifdef GTK3
		m = gtk_frame_new(title);
		c = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	#else
		GTX_TO_LATIN1(title);
		m = gtk_frame_new((const gchar*)l1_title);
		c = gtk_vbox_new(TRUE, 0);
	#endif
	gtk_container_add(GTK_CONTAINER(m), c);
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), m, TRUE, TRUE, 10);
#endif

	return regWidget(c);
}

} // extern "C"
