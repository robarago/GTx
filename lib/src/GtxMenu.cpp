/**
 * \file GtxMenu.cpp
 * Graphical Toolkit X for Modula-2: Menu, MenuBar, Popup and MenuItem
 * \author Roberto Aragón, Charo Baena
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget *	gwidget;
extern GtxWidget		gv;
extern GtxWidget		gmb; /// Vbox to hold toplevel menu bar
extern unsigned int ghasmenubar;
extern unsigned int gdebug;

#ifdef QT5
class GtxMenuItem : public QAction {
private:
	GtxReturn* data = NULL;
public:
	GtxMenuItem() : QAction(gv) {
		connect(this, static_cast<void (QAction::*)(bool)>(&QAction::triggered),
						this, static_cast<void (GtxMenuItem::*)(bool)>(&GtxMenuItem::callSuperCB));
		}
	void callSuperCB(bool b) {
		GTX_DBG("callSuperCB: %p\n", this->data);
		if(this->data) superCallback(this->parentWidget(), this->data, NULL);
	}
	void signal_connect(GtxReturn* data) { this->data = data; }
};
#else
#define GtxMenuItem "activate"
#endif

extern "C" {

#ifdef XAW
#define pb_width 12
#define pb_height 12
static unsigned char pb[] = {
   0x00, 0x00, 0x20, 0x00, 0x70, 0x00, 0xf8, 0x00, 0xfc, 0x01, 0xfe, 0x03,
   0x00, 0x00, 0xfe, 0x03, 0xfc, 0x01, 0xf8, 0x00, 0x70, 0x00, 0x20, 0x00};
Pixmap							pix;
#elif !defined(QT5)
static gint
button_press(GtkWidget* widget, GdkEvent* event)
{
	if(event->type == GDK_BUTTON_PRESS) {
		GdkEventButton *bevent = (GdkEventButton *)event;
		gtk_menu_popup(	GTK_MENU(widget), NULL, NULL, NULL, NULL,
										bevent->button, bevent->time);
		return TRUE;
	}
	return FALSE;
}
#endif

unsigned int
Popup(
	int box,
	char *label)
{
	GtxWidget p;
	GtxWidget b;
#ifdef XAW
	if(!pix) pix = XCreateBitmapFromData(	XtDisplay(gv),
																				RootWindowOfScreen(XtScreen(gv)),
																				(const char*)pb, pb_width, pb_height);
	b = XtVaCreateManagedWidget(label, menuButtonWidgetClass, gwidget[box],
															XtNleftBitmap, pix, 0, NULL);
	p = XtCreatePopupShell("menu", simpleMenuWidgetClass, b, NULL, 0);
#elif defined(QT5)
	b = new QPushButton;
	((QPushButton*)b)->setText(QString::fromUtf8(label));
	gwidget[box]->layout()->addWidget(b);
	p = new QMenu;
	((QPushButton*)b)->setMenu((QMenu*)p);
#else
	p = gtk_menu_new();
	b = gtk_button_new_with_label(label);
	gtk_box_pack_end(GTK_BOX(gwidget[box]), b, TRUE, TRUE, 0);
	gtx_sig_conn_obj(b, "event", button_press, p);
#endif
	GTX_DBG("Menu button %p registered\n", b);

	return regWidget(p);
}

unsigned int
MenuBar(
	int window)
{
	GtxWidget m;

	/// Pass window = 0 to set toplevel menu bar
#ifdef XAW
	if(!window) ghasmenubar = 1;
	m = XtVaCreateManagedWidget("menuBar", boxWidgetClass,
															window?gwidget[window]:gmb,
															XtNborderWidth, 0, XtNwidth, 1000,
															XtNhSpace, 0, XtNvSpace, 0, NULL);
#elif defined(QT5)
	m = new QMenuBar;
	if(0 == window) m = ((QMainWindow*)gv)->menuBar();
	else gwidget[window]->layout()->addWidget(m);
#else
	m = gtk_menu_bar_new();
	gtk_box_pack_start(GTK_BOX(window?gwidget[window]:gmb), m, FALSE, FALSE, 0);
#endif

	return regWidget(m);
}

unsigned int
Menu(
	int menubar,
	char *label)
{
	GtxWidget m;

#ifdef XAW
	Widget b = XtVaCreateManagedWidget(label, menuButtonWidgetClass,
																		gwidget[menubar], XtNborderWidth, 0, NULL);
	m = XtCreatePopupShell("menu", simpleMenuWidgetClass, b, NULL, 0);
#elif defined(QT5)
	m = new QMenu;
	QMenu* menu = (QMenu*)m;
	menu->setTitle(QString::fromUtf8(label));
	((QMenuBar*)gwidget[menubar])->addMenu(menu);
#else
	m = gtk_menu_new();
	GtkWidget *mi = gtk_menu_item_new_with_label(label);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(mi), m);
	gtk_menu_bar_append(GTK_MENU_BAR(gwidget[menubar]), mi);
#endif

	return regWidget(m);
}

GtxWidgetId
MenuItem(
	int menu,
	char *label,
	GtxEvent fn,
	void *data)
{
	GtxWidget mi;
	GtxReturn* ret;

#ifdef XAW
	mi = XtCreateManagedWidget(label, smeBSBObjectClass, gwidget[menu], NULL,0);
#elif defined(QT5)
	QAction* a = new GtxMenuItem;
	a->setText(QString::fromUtf8(label));
	((QMenu*)gwidget[menu])->addAction(a);	
	mi = a->parentWidget();
#else
	mi = gtk_menu_item_new_with_label(label);
	gtk_menu_append(GTK_MENU(gwidget[menu]), mi);
	gtk_widget_show(mi);
#endif
	ret = (GtxReturn*)createReturn(fn, label, mi, data, NULL, NULL);
#ifdef QT5
	((GtxMenuItem*)a)->signal_connect(ret);
#else
	gtx_sig_conn(mi, GtxMenuItem, superCallback, ret);
#endif

	return ret->id;
}

void
Separator(
	int menu)
{
#ifdef XAW
	XtCreateManagedWidget("", smeLineObjectClass, gwidget[menu], NULL,0);
#elif defined(QT5)
	((QMenu*)gwidget[menu])->addSeparator();	
#else
	GtxWidget mi = gtk_menu_item_new();
	gtk_menu_append(GTK_MENU(gwidget[menu]), mi);
	gtk_widget_show(mi);
#endif
}

} // extern "C"
