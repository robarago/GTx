/**
 * \file GtxButton.cpp
 * Graphical Toolkit X for Modula-2: Button, Toggle and friends
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget*		gwidget;
extern unsigned int	gdebug;

GTX_CONFIG_WIDGET(GtxButton, released, "clicked", QPushButton, void)

extern "C" {

GtxWidgetId
Button(
	int parent,
	char *label,
	GtxEvent fn,
	void *data)
{
	GtxWidget b;
	GtxReturn *ret;

	/// Add new button
#ifdef XAW
	GTX_TO_LATIN1(label)
	b = XtCreateManagedWidget((const char*)l1_label, commandWidgetClass,
		gwidget[parent], NULL, 0);
#elif defined(QT5)
	b = new GtxButton;
	((QPushButton*)b)->setText(QString::fromUtf8(label));
	gwidget[parent]->layout()->addWidget(b);
#else
	GTX_TO_LATIN1(label)
	GtkWidget* l = gtk_frame_new((const gchar*)l1_label);
	gtk_frame_set_shadow_type(GTK_FRAME(l), GTK_SHADOW_NONE);
	gtk_frame_set_label_align(GTK_FRAME(l), 0.5, 0.0);
	b = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(b), l);
	gtk_container_add(GTK_CONTAINER(gwidget[parent]), b);
#endif
	// Assign identification data for callback
	ret = createReturn(fn, label, b, data, NULL, NULL);
	// Connect callback function
	gtx_sig_conn(b, GtxButton, superCallback, ret);

	return ret->id;
}

} // extern "C"
