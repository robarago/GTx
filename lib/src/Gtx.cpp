/**
 * \file Gtx.cpp
 * Graphical Toolkit X for Modula-2: Main and common functions and data types
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

/// Globals
GtxWidget*		gwidget;	/// Big heap with all widget references
GtxWidget			gv;				/// Top widget window
GtxWidget			gmb;			/// Vbox for toplevel menu bar
unsigned int	ghasmenubar;
unsigned int	gdebug;		/// Debug enabled or disabled
unsigned int	gnc = 0;	/// Total number of controls

/// Locals
#ifdef XAW
XtAppContext	xt_app;
Widget				paned;
#elif defined(QT5)
GtxQApp*			app;
int						qt_argc;  /// Prevent optimization of argc
char**				qt_argv;  /// Prevent optimization of argv
#else
GdkAtom				prop, type;
gint					format, length;
#endif

extern "C" {

/**
 * Convert ISO-8859-1 string to UTF-8
 * Ensure that out pointer has twice
 * the length of the input string
 */
void
latin1_to_utf8(unsigned char* in, ///< Input (latin1) string
	unsigned char* out ///< Output (UTF-8) string
	)
{
	GTX_NOT_NULL(in, return)
	while(*in)
		if(*in < 128) *out++ = *in++;
		else *out++ = 0xc2 + (*in > 0xbf),
			*out++ = (*in++ & 0x3f) + 0x80;
	*out = '\0';
}

void
utf8_to_latin1(unsigned char* in, unsigned char* out) {
	GTX_NOT_NULL(in, return)
	while(*in)
		if(*in < 128) *out++ = *in++;
		else if(*in == 0xc2 || *in == 0xc3)
			*in++, *out++ = *in++ + 0x40 * (*(in - 2) > 0xc2);
	*out = '\0';
}

/**
 * Global widget registration
 * Dynamic creation of an item in the global array
 * to store necesary data to return to the callback
 * registered when the command was created
 * \return global widget identificator for later use
 */
GtxWidgetId
regWidget(GtxWidget w	///< Widget to register
	)
{
	if(NULL == w) {
		GTX_DBG("regWidget called with NULL widget\n");
		return -1;
	}
	gwidget = (GtxWidget *)realloc(gwidget, ++gnc * sizeof(GtxWidget));
	GTX_DBG("Widget registered [%d:%p]\n", gnc - 1, w);
	gwidget[gnc - 1] = w;

	return(gnc - 1);
}

/**
 * Create return data structure to superCallback & friends
 */
GtxReturn*
createReturn(
	GtxEvent fn,
	char *name,
	GtxWidget w,
	void *data,
	void *pdata,
	GtxCallback pfn)
{
	if(w == NULL) {
		GTX_ERR("Widget name=%s with NULL pointer\n", name);
		return NULL;
	}
	GtxReturn *ret = GTX_TYPE_MALLOC(GtxReturn);
	ret->fn = fn;
	ret->name = (char *)strdup(name);
	ret->id = regWidget(w);
	ret->data = data;
	ret->pdata = pdata;
	ret->pfn = pfn;
	GTX_DBG("Callback info: id=%d name=%s fn=%p data=%p pdata=%p pfn=%p\n",
				 ret->id, ret->name, ret->fn, ret->data, ret->pdata, ret->pfn);
	#if defined(GTK1) || defined(GTK2)
	char s[17] = "";
	sprintf(s, "%lx", ret);
	GTX_DBG("window property set: length=%d s=0x%s\n", length, s);
	gdk_property_change(w->window, prop, type, format,
		GDK_PROP_MODE_REPLACE, (guchar*)s, length);
	#endif

	return ret;
}

/**
 * Generic callback connected with every action widget (button, menu, etc).
 * It serves as a wrapper that calls return function with client data to the
 * proper Modula-2 callback procedure.
 */
void
superCallback(
	GtxWidget w,          ///< source widget generation event
	void*     data,       ///< puntero genérico recibido; siempre será \ref GtxReturn
	void*     clientData) ///< client data only in Xaw (unused in others)
{
	if(NULL == data) {
		GTX_ERR("NULL data pointer in function call\n");
		return;
	}
	GtxReturn *ret = (GtxReturn *)data;
	if(NULL != ret->pfn) {
		GTX_DBG("Private event from widget# %d (%s) with callback %p and data %p\n", 
            ret->id, ret->name, ret->pfn, ret->data);
		(*ret->pfn)(gwidget[ret->id], data, NULL);
	}
	GTX_DBG("Event from widget# %d (%s) with callback %p and data %p\n",
					ret->id, ret->name, ret->fn, ret->data);
	(*ret->fn)(ret->id, ret->name, strlen(ret->name), ret->data);
}

#ifdef GLADE
void gladeXMLConnectFunc(
	const gchar *handler_name,
	GtxObject *object,
  const gchar *signal_name,
  const gchar *signal_data,
  GtxObject *connect_object,
  gboolean after,
  gpointer user_data)
{
	if(NULL == object) return;
	GtkWidget *w = (GtkWidget *)object;
	static GModule *module = NULL;
	gpointer symbol = NULL;
	char* wname = NULL;

#ifdef GTK3
	wname = (char*)gtk_widget_get_name(w);
#else
	wname = w->name;
#endif

	GTX_DBG("Connecting...\n\twidget: %p (%s)\n\thandler: %s\n\tsignal_name: %s\n"\
		"\tsignal_data: %s\n\tconnect_object: %p\n\tuser_data: %p\n",
		object, wname, handler_name, signal_name,
		signal_data, connect_object, user_data);

	module = g_module_open(NULL, (GModuleFlags)0);
	if(!module) {
		GTX_ERR("Invalid module handle: %s\n", g_module_error());
		return;
	}
	if(!g_module_symbol(module, handler_name, (gpointer *)&symbol)) {
		GTX_ERR("Could not find signal handler: %s\n", g_module_error());
		return;
	}
	if(handler_name[0] == 'g' && handler_name[1] == 't' && handler_name[2] == 'k')
		gtx_sig_conn(object, signal_name, symbol, user_data);
	else {
		GtxReturn *ret = createReturn((GtxEvent)symbol, wname, (GtxWidget)w,
										(void *)signal_data, NULL, NULL);
		gtx_sig_conn(object, signal_name, superCallback, ret);
	}
}
#endif

/**
 * SuperAction and quit for entries and window close command
 */
#ifdef XAW
void
hideCaret(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	int i = 0;
	do {
		if(XtClass(gwidget[i]) == asciiTextWidgetClass)
			XtVaSetValues(gwidget[i], XtNdisplayCaret, 0, NULL);
	}
	while(++i < gnc);
}

void
superAction(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	GtxReturn *ret;
	unsigned long l;
	unsigned int i;

	if(!strcmp(params[0], "return")) {
		sscanf(params[1], "%ld", &l); 
		ret = (GtxReturn *)l;
		superCallback(w, ret, NULL);
	}
	if(!strcmp(params[0], "rtab")) {
		sscanf(params[1], "%d", &i); 
		int e = i;
		for( ;; ) {
			e = (e + 1) % gnc;
			if(e == i) break;
			if(XtClass(gwidget[e]) == asciiTextWidgetClass) {
				XtSetKeyboardFocus(gv, gwidget[e]);
				XawTextSetInsertionPoint(gwidget[e], XawTextLastPosition(gwidget[e]));
				break;
			}
		}
		XtVaSetValues(gwidget[e], XtNdisplayCaret, 1, NULL);
	}
	if(!strcmp(params[0], "ltab")) {
		sscanf(params[1], "%d", &i); 
		int e = i;
		for( ;; ) {
			e = (e - 1) < 0 ? gnc - 1 : (e - 1) % gnc;
			if(e == i) break;
			if(XtClass(gwidget[e]) == asciiTextWidgetClass) {
				XtSetKeyboardFocus(gv, gwidget[e]);
				XawTextSetInsertionPoint(gwidget[e], XawTextLastPosition(gwidget[e]));
				break;
			}
		}
		XtVaSetValues(gwidget[e], XtNdisplayCaret, 1, NULL);
	}
}

void
quit(Widget w, XEvent *event, String *params, Cardinal *num_params) {
	XtDestroyApplicationContext(xt_app);
	exit(0);
}

static XtActionsRec actions[] = {
	{ (char*)"quit", quit },
	{ (char*)"superAction", superAction },
	{ (char*)"hide-caret", hideCaret }
};

#elif !defined(QT5)
void
quit(GtkWidget *w, gpointer data) {
  gtk_main_quit();
}
#endif

/**
 * Main event loop: show main window (widget) and all descendants
 * before entering dispatch event loop.
 */
void
MainLoop()
{
#ifdef XAW
	XtRealizeWidget(gv);
	if(!ghasmenubar) XtVaSetValues(paned, XtNinternalBorderWidth, 0, NULL);
	protect_close(gv);
	// Set keyboard focus on first entry if exists
	for(int i = 0; i < gnc; i++) {
		if(XtClass(gwidget[i]) == asciiTextWidgetClass) {
			GTX_DBG("Focus now on widget: [%d:0x%x]\n", i, gwidget[i]);
			XawTextSetInsertionPoint(gwidget[i], XawTextLastPosition(gwidget[i]));
			XawTextDisplayCaret(gwidget[i], True);
			XtSetKeyboardFocus(gv, gwidget[i]);
			break;
		}
	}
	XtAppMainLoop(xt_app);
#elif defined(QT5)
	QApplication::exec();
	GTX_DBG("Application exited\n");
#else
	gtk_widget_show_all(gv);
	gtk_main();
#endif
}

/**
 * Toolkit inicialization and main window creation
 */
unsigned int
MainInit(
	char *classname, ///< main application class name
	char *title, ///< main window title
	unsigned int d, ///< debug flag: 1-enabled, 0-disabled
	int argc, ///< pass program argc here
	char* argv[] ///< pass program argv here
	)
{
	GtxWidget m;

	gdebug = d;
	ghasmenubar = 0;
#ifdef XAW
	gv = XtVaAppInitialize(&xt_app, classname, NULL, 0, &argc, argv, NULL, NULL);
	XtVaSetValues(gv, XtNtitle, title, NULL);
	// Horrible hack to avoid abrupt window closing
	XtAppAddActions(xt_app, actions, XtNumber(actions));
	XtOverrideTranslations(gv, XtParseTranslationTable("<Message>WM_PROTOCOLS: quit()"));
	paned = XtVaCreateManagedWidget("container", panedWidgetClass, gv,
																	XtNborderWidth, 0, XtNhSpace, 0,
																	XtNgripIndent, -10,
																	XtNvSpace, 0, NULL);
	gmb = XtVaCreateManagedWidget("menubar", boxWidgetClass, paned,
																XtNborderWidth, 0, XtNhSpace, 0,
																XtNvSpace, 0, NULL);
	m = XtVaCreateManagedWidget("contents", boxWidgetClass, paned,
															XtNborderWidth, 0, XtNheight, 1000, NULL);
#elif defined(QT5)
	Q_UNUSED(classname);
	qt_argc = argc;
	qt_argv = argv;
	app = new GtxQApp(qt_argc, qt_argv);
	gv = new QMainWindow; // Top level window
	gv->setWindowTitle(title); // Set top window title
	m = new QWidget; // Contents window
	QVBoxLayout *b = new QVBoxLayout;
	m->setLayout(b);
	((QMainWindow*)gv)->setCentralWidget(m);
	gv->show();
#else
	gtk_init(&argc, &argv);
	gtk_set_locale();
	// Init set/get window property atoms and format
	prop = gdk_atom_intern("CBDATA", FALSE);
	type = gdk_atom_intern("POINTER", FALSE);
	format = 8; /// 8 x 16 = 64 bits pointers
	length = 16;
	#ifdef GLADE
	if(classname[0] != '\0') {
		char gladefile[80];
		sprintf(gladefile, "%s.glade", classname);
		GTX_DBG("Glade file: %s\n", gladefile);
		if(-1 != access(gladefile, F_OK)) {
			glade_init();
			GladeXML *xml = gtx_glade_xml_new(gladefile);
			glade_xml_signal_autoconnect_full(xml, gladeXMLConnectFunc, NULL);
			// IMPORTANT NOTE: main_window name MUST be == `classname' also
			GtkWidget *x = glade_xml_get_widget(xml, classname);
			GTX_DBG("Glade GtkWidget: %x\n", x);
			m = gv = x;
			gtx_sig_conn(gv, "destroy", quit, NULL);	
			return regWidget(x);
		}
	}
	#endif
	gv = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(gv), title);
	// Container vbox
	#ifdef GTK3
	GtkWidget *c = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	#else
	GtkWidget *c = gtk_vbox_new(FALSE, 0);
	#endif
	gtk_container_add(GTK_CONTAINER(gv), c);
	// Menu bar vbox
	#ifdef GTK3
	gmb = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	#else
	gmb = gtk_vbox_new(FALSE, 0);
	#endif
	gtk_box_pack_start(GTK_BOX(c), gmb, TRUE, TRUE, 0);
	// Contents vbox (main window)
	#ifdef GTK3
	m = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	#else
	m = gtk_vbox_new(FALSE, 0);
	#endif
	gtk_container_set_border_width(GTK_CONTAINER(m), 10);
	gtk_box_pack_end(GTK_BOX(c), m, TRUE, TRUE, 0);
	gtx_sig_conn(gv, "destroy", quit, NULL);	
#endif

	return regWidget(m);
}

/**
 * Get selection payload data from callback information
 */
#ifndef QT5
GtxReturn*
getReturnData(GtxWidget w)
{
	GtxReturn* ret = NULL;
#if defined(XAW)
	XtCallbackList cbl;
	XtVaGetValues(w, XtNcallback, &cbl, NULL);
	ret = (GtxReturn*)cbl->closure;
#else
	GdkAtom outtype;
	guchar* s = NULL;
	#ifdef GTK1
	if(!gdk_property_get(w->window, prop, type, 0, 16, FALSE, &outtype,
			&format, &length, &s)) {
	#else
	if(!gdk_property_get(gtk_widget_get_window(w), prop, type, 0, 16, FALSE, &outtype,
			&format, &length, &s)) {
	#endif
		GTX_DBG("Property CBDATA of type POINTER can't be retrieved - call ignored\n");
		return NULL;
	}
	GTX_DBG("window property get: length=%d s=0x%s\n", length, s);
	sscanf((const char*)s, "%lx", &ret);
	g_free(s);
#endif
	return ret;
}
#endif

} // extern "C"
