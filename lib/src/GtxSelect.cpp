/**
 * \file GtxSelect.cpp
 * Graphical Toolkit X for Modula-2: selection widgets Option and Check
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget	gv;
extern GtxWidget* gwidget;
extern int				gdebug;

GTX_CONFIG_WIDGET(GtxCheck, stateChanged, "clicked", QCheckBox, int)
GTX_CONFIG_WIDGET(GtxOption, released, "clicked", QRadioButton,)

/**
 * Augmented return type for seleccion callback
 */
typedef struct {
	enum { Option, Check = 2 } seltype;
	unsigned int active;
	GtxWidget group;
	GtxWidget selwidget;
} GtxSelection;

/**
 * Support decorations for check and option buttons
 */
#ifdef XAW
#define decors_width 12
#define decors_height 12
static unsigned const char decors[][decors_width * decors_height] = {
	{	0x00, 0x00, 0x20, 0x00, 0x50, 0x00, 0x88, 0x00,
		0x04, 0x01, 0x02, 0x02, 0x01, 0x04, 0x02, 0x02,
		0x04, 0x01, 0x88, 0x00, 0x50, 0x00, 0x20, 0x00 },//0: optionoff
	{	0x00, 0x00, 0x20, 0x00, 0x50, 0x00, 0xa8, 0x00,
		0x74, 0x01, 0xfa, 0x02, 0xfd, 0x05, 0xfa, 0x02,
		0x74, 0x01, 0xa8, 0x00, 0x50, 0x00, 0x20, 0x00 },//1: optionon
	{	0x00, 0x00, 0xff, 0x1f, 0x01, 0x18, 0x01, 0x18,
		0x01, 0x18, 0x01, 0x18, 0x01, 0x18, 0x01, 0x18,
		0x01, 0x18, 0x01, 0x18, 0x01, 0x18, 0xff, 0x1f },//2: checkoff
	{	0x00, 0x00, 0xff, 0x2f, 0x01, 0x18, 0x01, 0x0c,
		0x01, 0x0e, 0x05, 0x0f, 0x8f, 0x0b, 0xdd, 0x09,
		0xf9, 0x08, 0x71, 0x08, 0x21, 0x08, 0xff, 0x0f } //3: checkon
};
Pixmap decorbits[4];
#endif

extern "C" {

#ifdef XAW
/**
 * Change selection status of widget according to specified state:
 * also unselect previously selected widget if it belongs to a group.
 */
void
toggleSelect(Widget w, void* d, void* c)
{
	GtxReturn *ret = getReturnData(w);
	GtxSelection *sel = (GtxSelection*)ret->pdata;
	if(sel->seltype == GtxSelection::Option) {
		// Work with group leader selection data
		GtxWidget group = ((GtxSelection*)getReturnData(w)->pdata)->group;
		GtxSelection* gsel = (GtxSelection*)getReturnData(group)->pdata;
		/// Get previously selected widget in group
		GtxWidget unselected = gsel->selwidget;
		/// Change decoration and status to unselected
		XtVaSetValues(unselected, XtNleftBitmap, decorbits[0], 0, NULL);
		/// Change decoration and status to selected
		XtVaSetValues(w, XtNleftBitmap, decorbits[1], 0, NULL);
		/// Store this widget as selected in group leader's selection data
		gsel->selwidget = w;
	}
	else {
		sel->active = !sel->active;
		XtVaSetValues(w, XtNleftBitmap, decorbits[GtxSelection::Check + sel->active], 0, NULL);
	}
}

/**
 * Load bitmaps, better choice than use M2_module_init
 */
void init() {
	for(int i = 0; i < 4; i++) {
		decorbits[i] = XCreateBitmapFromData(XtDisplay(gv),
			RootWindowOfScreen(XtScreen(gv)), (const char*)decors[i], decors_width,
			decors_height);
	}
}
#endif

/**
 * Gets current option/check status
 */
unsigned int
isSelected(
	int widget)
{
#ifdef XAW
	GtxSelection* sel = (GtxSelection*)getReturnData(gwidget[widget])->pdata;
	if(sel->seltype == GtxSelection::Option)
		return gwidget[widget] == ((GtxSelection*)getReturnData(sel->group)->pdata)->selwidget;
	else
		return sel->active;
#elif defined(QT5)
	return ((QAbstractButton*)gwidget[widget])->isChecked();
#else
	return GTK_TOGGLE_BUTTON(gwidget[widget])->active;
#endif
}

/**
 * Xt: Option button is a command button with same look and different behaviour.
 * Look: an small icon on the right of the button will be toogled.
 * Behaviour: when one button in the group is checked, the rest are unchecked.
 */
GtxWidgetId
Option(
	int parent,
	char *label,
	unsigned int checked,
	int group,
	GtxEvent fn,
	void *data)
{
	GtxWidget o;
	GtxReturn *ret;
	GtxSelection *sel = GTX_TYPE_MALLOC(GtxSelection);
	void (*pfn)(GtxWidget, void*, void*) = NULL;

	/// If first of group, reuse ret->id in next widget group
	if(!group) checked = 1;
#ifdef XAW
	if(!decorbits[0]) init();
	o = XtVaCreateManagedWidget(label, commandWidgetClass, gwidget[parent],
															XtNborderWidth, 0,
															XtNleftBitmap, decorbits[checked], NULL);
	sel->seltype = GtxSelection::Option;
	sel->active = checked;
	if(!group) sel->group = sel->selwidget = o;
	else {
		GtxWidget leader = gwidget[group];
		sel->group = leader;
		if(checked) { // not group leader and checked: change selected widget
			// Get current selection data from group leader
			GtxSelection* gsel = (GtxSelection*)getReturnData(leader)->pdata;
			/// Change decoration and status of previous selected widget
			XtVaSetValues(gsel->selwidget, XtNleftBitmap, decorbits[0], 0, NULL);
			/// Change decoration and status to selected
			XtVaSetValues(o, XtNleftBitmap, decorbits[1], 0, NULL);
			/// Store this widget as selected in group leader's selection data
			gsel->selwidget = o;
		}
	}
	pfn = toggleSelect;
#elif defined(QT5)
	o = new GtxOption;
	QRadioButton* r = (QRadioButton*)o;
	r->setText(QString::fromUtf8(label));
	r->setParent(gwidget[parent]);
	r->setChecked(checked);
	gwidget[parent]->layout()->addWidget(r);
#else
	o = gtk_radio_button_new_with_label_from_widget(
			group ? GTK_RADIO_BUTTON(gwidget[group]) : NULL, label);
	gtk_container_add(GTK_CONTAINER(gwidget[parent]), o);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(o), checked);
#endif
	// Assign identification data for callback
	ret = createReturn(fn, label, o, data, sel, pfn);
	GTX_DBG("* Widget %p connected with callback %p and data %p\n",
		o, superCallback, ret);
	// Connect widget with callback
	gtx_sig_conn(o, GtxOption, superCallback, ret);

	return ret->id;
}

GtxWidgetId
Check(
	int parent,
	char *label,
	unsigned int checked,
	GtxEvent fn,
	void *data)
{
	GtxWidget c;
	GtxReturn *ret;
	GtxSelection *sel = GTX_TYPE_MALLOC(GtxSelection);
	void (*pfn)(GtxWidget, void*, void*) = NULL;

#ifdef XAW
	if(!decorbits[0]) init();
	sel->seltype = GtxSelection::Check;
	sel->active = checked;
	pfn = toggleSelect;
	c = XtVaCreateManagedWidget(label, commandWidgetClass, gwidget[parent],
															XtNborderWidth, 0, XtNleftBitmap,
															decorbits[sel->seltype + checked], NULL);
#elif defined(QT5)
	c = new GtxCheck;
	QCheckBox* b = (QCheckBox*)c;
	b->setText(QString::fromUtf8(label));
	b->setParent(gwidget[parent]);
	b->setChecked(checked);
	gwidget[parent]->layout()->addWidget(b);
#else
	c = gtk_check_button_new_with_label(label);
	gtk_container_add(GTK_CONTAINER(gwidget[parent]), c);
#endif
	// Assign identification data for callback
	ret = createReturn(fn, label, c, data, sel, pfn);
	// Connect widget with callback
	GTX_DBG("Widget %p connected with callback %p and data %p\n", c,
					superCallback, ret);
	gtx_sig_conn(c, GtxCheck, superCallback, ret);

	return ret->id;
}

} // extern "C"
