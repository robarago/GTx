/**
 * \file GtxText.cpp
 * Graphical Toolkit X for Modula-2: Entry, TextPane, Label and accesory functions
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern GtxWidget*	gwidget;
extern unsigned int gdebug;

GTX_CONFIG_WIDGET(GtxEntry, returnPressed, "", QLineEdit,)

extern "C" {

unsigned int
Label(
	int parent,
	char *label)
{
	GtxWidget l;

#ifdef XAW
	GTX_TO_LATIN1(label)
	l = XtVaCreateManagedWidget(label, labelWidgetClass, gwidget[parent],
			XtNlabel, (const char*)l1_label, XtNborderWidth, 0, NULL);
#elif defined(QT5)
	l = new QLabel(QString::fromUtf8(label));
	gwidget[parent]->layout()->addWidget(l);
#elif defined(GTK3)
	l = gtk_label_new(label);
	gtk_widget_set_halign(GTK_MISC(l), GTK_ALIGN_START);
	gtk_widget_set_valign(GTK_MISC(l), GTK_ALIGN_START);
	gtk_label_set_line_wrap(GTK_LABEL(l), FALSE);
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), l, TRUE, TRUE, 10);
#else
	#ifdef GTK1
	GTX_TO_LATIN1(label);
	l = gtk_frame_new((const gchar*)l1_label);
	gtk_frame_set_shadow_type(GTK_FRAME(l), GTK_SHADOW_NONE);
	#else
	l = gtk_label_new((const gchar*)label);
	gtk_misc_set_alignment(GTK_MISC(l), 0.0, 0.0);
	#endif
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), l, TRUE, TRUE, 10);
#endif

	return regWidget(l);
}

void
setLabelText(
	int label,
	char *text)
{
#ifdef XAW
	GTX_TO_LATIN1(text);
	XtVaSetValues(gwidget[label], XtNlabel, l1_text, NULL);
#elif defined(QT5)
	QLabel *l = (QLabel *)gwidget[label];
	l->setText(QString::fromUtf8(text));
#elif defined(GTK1)
	GTX_TO_LATIN1(text);
	gtk_frame_set_label(GTK_FRAME(gwidget[label]), (const gchar*)l1_text);
#else
	gtk_label_set_text(GTK_LABEL(gwidget[label]), (const gchar*)text);
#endif
}

GtxWidgetId
Entry(
	int parent,
	char *label,
	int maxlength,
	char *text, /// Can be NIL
	GtxEvent fn,
	void *data)
{
	GtxWidget m;
	GtxWidget l;
	GtxWidget t;
	GtxReturn *ret;

#ifdef XAW
	GTX_TO_LATIN1(text);
	m = XtVaCreateManagedWidget("entryBox", boxWidgetClass, gwidget[parent],
															XtNborderWidth, 0, XtNwidth, 1000, NULL);
	l = XtVaCreateManagedWidget(label, labelWidgetClass, m, XtNborderWidth, 0, NULL);
	t = XtVaCreateManagedWidget("entry", asciiTextWidgetClass, m,
															XtNlength, maxlength, XtNeditType, XawtextEdit,
															XtNdisplayCaret, 0, XtNstring, (const char*)l1_text,
															XtNwidth, 8 * maxlength, NULL);
#elif defined(QT5)
	m = new QWidget;
	QHBoxLayout* b = new QHBoxLayout;
	m->setLayout(b);
	l = new QLabel(QString::fromUtf8(label));
	((QLabel *)l)->setAlignment(Qt::AlignLeft);
	l->setMinimumWidth(strlen(label) * 8);
	b->addWidget(l);
	GtxEntry* e = new GtxEntry;
	((QLineEdit*)e)->setText(QString::fromUtf8(text));
	e->setMaxLength(maxlength);
	e->setMinimumWidth(maxlength * 8 + 2);
	t = (QWidget *)e;
	b->addWidget(t);
	gwidget[parent]->layout()->addWidget(m);
#else
	m = gtk_hbox_new(TRUE, 10);
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), m, TRUE, TRUE, 5);
	if(maxlength > 0) {
		#ifdef GTK1
			GTX_TO_LATIN1(label);
			l = gtk_label_new((const gchar*)l1_label); // Doesn't work with latin1 nor UTF-8
			gtk_frame_set_shadow_type(GTK_FRAME(l), GTK_SHADOW_NONE);
			gtk_frame_set_label_align(GTK_FRAME(l), 1.0, 0.0);
		#else
			l = gtk_label_new(label);
			gtk_label_set_justify(GTK_LABEL(l), GTK_JUSTIFY_LEFT);
			gtk_label_set_line_wrap(GTK_LABEL(l), FALSE);
		#endif
		gtk_box_pack_start(GTK_BOX(m), l, TRUE, TRUE, 0);
	}
	t = gtk_entry_new_with_max_length(maxlength);
	if(text != NULL) {
	#ifdef GTK1
		GTX_TO_LATIN1(text);
		gtk_entry_set_text(GTK_ENTRY(t), (const gchar*)l1_text);
	#else
		gtk_entry_set_text(GTK_ENTRY(t), text);
	#endif
	}
#endif

	// Return data preparation for entry callback
	ret = createReturn(fn, label, t, data, NULL, NULL);

#ifdef XAW
	char entryAction[1024];
	sprintf(entryAction, "<Key>Return: superAction(return,%ld)\n"
		"<Btn1Down>: hide-caret() set-keyboard-focus() display-caret()\n"
		"Shift<Key>Tab: hide-caret() superAction(ltab,%d)\n"
		"<Key>Tab: hide-caret() superAction(rtab,%d)\n", (long int)ret,
		ret->id, ret->id);
	XtOverrideTranslations(t, XtParseTranslationTable(entryAction));
#elif defined(QT5)
	gtx_sig_conn(t, GtxEntry, superCallback, ret);
#else
	gtk_signal_connect(GTK_OBJECT(t), "activate", GTK_SIGNAL_FUNC(superCallback), (gpointer)ret);
	gtk_box_pack_start(GTK_BOX(m), t, TRUE, TRUE, 5);
#endif

	return ret->id;
}

void
getEntryText(
	int entry,
	char *text,
	int maxlength)
{
#ifdef XAW
	String string;
	XtVaGetValues(gwidget[entry], XtNstring, &string, NULL);
	GTX_TO_UTF8(string);
	strncpy(text, (const char*)u8_string, maxlength);
#elif defined(QT5)
	QLineEdit *e = (QLineEdit *)gwidget[entry];
	strncpy(text, e->text().toUtf8().data(), maxlength);
#else
	gchar *entrytext;
	entrytext = (gchar *)gtk_entry_get_text(GTK_ENTRY(gwidget[entry]));
	#ifdef GTK1
		GTX_TO_UTF8(entrytext);
		strncpy(text, (const char*)u8_entrytext, maxlength);
	#else
		strncpy(text, entrytext, maxlength);
	#endif
#endif
}

void
setEntryText(
	int entry,
	char *text)
{
	GtxWidget t = gwidget[entry];
	GTX_TO_LATIN1(text);
#ifdef XAW
	XtVaSetValues(t, XtNstring, l1_text, NULL);
#elif defined(QT5)
	QLineEdit *e = (QLineEdit *)t;
	e->setText(QString::fromUtf8(text));
#elif defined(GTK1)
	gtk_entry_set_text(GTK_ENTRY(t), (const gchar*)l1_text);
#else
	gtk_entry_set_text(GTK_ENTRY(t), (const gchar*)text);
#endif
}

} // extern "C"
