/**
 * \file GtxList.cpp
 * Graphical Toolkit X for Modula-2: List widget and related functions
 * \author Roberto Aragón
 * \version $Id$
 */

#include "Gtx.h"

extern unsigned int	gdebug;
extern GtxWidget*		gwidget;
extern GtxWidget		gv;

GTX_CONFIG_WIDGET(GtxList, itemSelectionChanged, "selection-changed", QListWidget,)
GTX_CONFIG_WIDGET(GtxCombo, currentIndexChanged, "selection-changed", QComboBox, int)

#ifdef XAW
	#define cbpixmap_width 10
	#define cbpixmap_height 14
	static const unsigned char cbpixmap_bits[] = {
		 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		 0xfe, 0x01, 0xfe, 0x01, 0xfc, 0x00, 0x78, 0x00, 0x30, 0x00, 0x00, 0x00,
		 0x00, 0x00, 0x00, 0x00};
	Pixmap cbpixmap;
	typedef struct {
		Widget entry;
		Widget list;
		Widget shell;
	} GtxComboList;
#elif !defined(QT5)
	typedef struct {
		const gchar *label;
		unsigned int index;
	} GtxListItemData;
#endif

extern "C" {

GtxWidgetId
List(
	int parent,
	unsigned int columns,
	GtxEvent fn,
	void *data)
{
	GtxWidget l;
	GtxReturn *ret;

#ifdef XAW
	Widget v = XtVaCreateManagedWidget("viewport", viewportWidgetClass,
																			gwidget[parent], XtNallowVert, True,
																			XtNheight, 100, XtNuseRight, True, NULL);
	l = XtVaCreateManagedWidget(NULL, listWidgetClass, v, XtNheight, 1,
															XtNwidth, 1, XtNdefaultColumns, columns, NULL);
#elif defined(QT5)
	l = new GtxList;
	gwidget[parent]->layout()->addWidget(l);
#else
	l = gtk_list_new();
	unsigned int *list_count = GTX_TYPE_MALLOC(unsigned int);
	*list_count = 0;
	gtk_object_set_data(GTK_OBJECT(l), "list_count", list_count);
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), l, TRUE, TRUE, 0);
#endif
	// Assign identification data for callback
	ret = createReturn(fn, (char*)"list", l, data, NULL, NULL);
	// Connect widget with callback
	gtx_sig_conn(l, GtxList, superCallback, ret);

	return ret->id;
}

void
addItem(
	int list,
	unsigned char* item)
{
#ifdef XAW
	int i, nstrings;
	String *strings;
	XtVaGetValues(gwidget[list], XtNlist, &strings, NULL);
	XtVaGetValues(gwidget[list], XtNnumberStrings, &nstrings, NULL);
	if(nstrings == 1 && strings && strings[0][0] == '\0') nstrings = 0;
	if(nstrings == 0) strings = NULL;
	strings = (String *)realloc(strings, 1 + nstrings * sizeof(String *));
	strings[nstrings++] = strdup((const char*)item);
	XawListChange(gwidget[list], (const char**)strings, nstrings, -1, True);
#elif defined(QT5)
	if(NULL != qobject_cast<QComboBox*>(gwidget[list]))
		((QComboBox*)gwidget[list])->addItem(
			QString::fromUtf8((const char*)item));
	else
		((QListWidget*)gwidget[list])->addItem(
			QString::fromUtf8((const char*)item));
#else
	GList *items = NULL;
	GtkWidget *i = gtk_list_item_new_with_label((const gchar*)item);
	gtk_widget_show(i);
	GtxListItemData *lid = GTX_TYPE_MALLOC(GtxListItemData);
	unsigned int *list_count = (unsigned int *)
		gtk_object_get_data(GTK_OBJECT(gwidget[list]), "list_count");
	lid->label = strdup((const char*)item);
	lid->index = (*list_count)++;
	gtk_object_set_data(GTK_OBJECT(i), "list_item_data", lid);
	items = g_list_append(items, i);
	gtk_list_append_items(GTK_LIST(gwidget[list]), items);
#endif
}

void
getSelectedItem(
	int list,
	char* itemtext,
	int* index)
{
#ifdef XAW
	XawListReturnStruct* lrs = XawListShowCurrent(gwidget[list]);
	strcpy((char*)itemtext, (const char*)lrs->string);
	*index = lrs->list_index;
#elif defined(QT5)
	if(NULL != qobject_cast<QComboBox*>(gwidget[list])) {
		QComboBox* b = (QComboBox*)gwidget[list];
		*index = b->currentIndex();
		strcpy((char*)itemtext, (const char*)b->currentText().toUtf8());
	}
	else {
		QListWidget* w = (QListWidget*)gwidget[list];
		strcpy((char*)itemtext, (const char*)w->currentItem()->text().toUtf8());
		*index = w->currentRow();
	}
#else
	GList* items = GTK_LIST(gwidget[list])->selection;
	if(!items) {
		strcpy((char*)itemtext, "");
		return;
	}
	GtkObject* item = GTK_OBJECT(items->data);
	GtxListItemData* lid = (GtxListItemData*)gtk_object_get_data(item, "list_item_data");
	strcpy((char*)itemtext, lid->label);
	*index = lid->index;
#endif
	GTX_DBG("Item selected: %s (%d)\n", itemtext, *index);
}

#ifdef XAW
void
superCBCallback(
	Widget w,
	XtPointer clientdata,
	XtPointer calldata)
{
	GtxComboList *cb = (GtxComboList *)clientdata;
	XtPopup(cb->shell, XtGrabExclusive);
}
void
popupCBCallback(
	Widget w,
	XtPointer clientdata,
	XtPointer calldata)
{
	Position x, y;
	Dimension height;
	GtxComboList *cb = (GtxComboList *)clientdata;
	XtVaGetValues(cb->entry, XtNheight, &height, NULL);
	XtTranslateCoords(cb->entry, (Position)0, (Position)0, &x, &y);
	XtVaSetValues(cb->shell, XtNx, x - 1, XtNy, y + height, NULL);
}
void
popdownCBCallback(
	Widget w,
	XtPointer clientdata,
	XtPointer calldata)
{
	GtxComboList *cb = (GtxComboList *)clientdata;
	XawListReturnStruct *lrs = XawListShowCurrent(cb->list);
	XtVaSetValues(cb->entry, XtNstring, lrs->string, NULL);
	XtPopdown(cb->shell);
}
#endif

GtxWidgetId
ComboList(
	int parent,
	int width,
	int height,
	GtxEvent fn,
	void *data)
{
	GtxWidget c;
	GtxWidget l;
	GtxWidget t;
	GtxReturn *ret;

#ifdef XAW
	if(!cbpixmap)
		cbpixmap = XCreateBitmapFromData(	XtDisplay(gv),
																			RootWindowOfScreen(XtScreen(gv)),
																			(const char*)cbpixmap_bits, cbpixmap_width,
																			cbpixmap_height);
  Widget m = XtVaCreateManagedWidget(	"cbBox", boxWidgetClass,
																			gwidget[parent], XtNborderWidth, 0,
																			XtNwidth, 1000, XtNhSpace, 1, NULL);
  t = XtVaCreateManagedWidget("cbEntry", asciiTextWidgetClass, m,
															XtNeditType, XawtextEdit,
															XtNdisplayCaret, 0,
															XtNwidth, width, NULL);
	Widget b = XtVaCreateManagedWidget( "cbButton", commandWidgetClass, m,
																			XtNbitmap, cbpixmap, NULL);
	Widget s = XtCreatePopupShell("cbShell", overrideShellWidgetClass, b,
																NULL, 0);
	Widget v = XtVaCreateManagedWidget("cbListViewport", viewportWidgetClass, s,
																			XtNallowVert, True,
																			XtNheight, height,
																			XtNwidth, width,
																			XtNuseRight, True,
																			NULL);
	l = XtVaCreateManagedWidget(NULL, listWidgetClass, v,
															XtNheight, 1, XtNwidth, 1,
															XtNdefaultColumns, 1, NULL);
	GtxComboList *cb = GTX_TYPE_MALLOC(GtxComboList);
	cb->list = l;
	cb->entry = t;
	cb->shell = s;
	XtAddCallback(b, XtNcallback, superCBCallback, cb);
	XtAddCallback(s, XtNpopupCallback, popupCBCallback, cb);
	XtAddCallback(l, XtNcallback, popdownCBCallback, cb);
#elif defined(QT5)
	l = t = c = new GtxCombo;
	((QComboBox*)c)->setEditable(true);
	gwidget[parent]->layout()->addWidget(c);
#else
	c = gtk_combo_new();
	l = GTK_COMBO(c)->list;
	t = GTK_COMBO(c)->entry;
	unsigned int *list_count = GTX_TYPE_MALLOC(unsigned int);
	*list_count = 0;
	gtk_object_set_data(GTK_OBJECT(l), "list_count", list_count);
	gtk_box_pack_start(GTK_BOX(gwidget[parent]), c, TRUE, TRUE, 0);
#endif
	// Register also text entry
	regWidget(t);
	// Assign identification data for callback
	ret = createReturn(fn, (char*)"combolist", l, data, NULL, NULL);
	// Connect widget with callback
	gtx_sig_conn(l, GtxCombo, superCallback, ret);

	return ret->id;
}

void
getComboListText(
	int combolist,
	char *text,
	unsigned int maxlength)
{
	const char *p;
#ifdef XAW
	XtVaGetValues(gwidget[combolist - 1], XtNstring, &p, NULL);
#elif defined(QT5)
	QComboBox* c = (QComboBox*)gwidget[combolist - 1];
	p = c->currentText().toUtf8();
#else
	p = gtk_entry_get_text(GTK_ENTRY(gwidget[combolist - 1]));
#endif
	strncpy(text, p, maxlength);
	GTX_DBG("Combo text: %s\n", text);
}

} // extern "C"
