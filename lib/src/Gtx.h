/**
 * \file Gtx.h
 * Graphical Toolkit X for Modula-2: Includes, macros, constant, globals & data types
 * \author Roberto Arag�n
 * \version $Id$
 */
#ifndef __GTX_H__
#define __GTX_H__

/// Includes

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#ifdef XAW
	#include <libgen.h>
	#include <dirent.h>
	#include <fnmatch.h>
	#include <string.h>
	#include <sys/stat.h>
	#include <X11/Intrinsic.h>
	#include <X11/StringDefs.h>
	#include <X11/Shell.h>
	#include <X11/Xaw/AsciiText.h>
	#include <X11/Xaw/Command.h>
	#include <X11/Xaw/Form.h>
	#include <X11/Xaw/Box.h>
	#include <X11/Xaw/Dialog.h>
	#include <X11/Xaw/List.h>
	#include <X11/Xaw/Label.h>
	#include <X11/Xaw/Viewport.h>
	#include <X11/Xaw/MenuButton.h>
	#include <X11/Xaw/SimpleMenu.h>
	#include <X11/Xaw/SmeBSB.h>
	#include <X11/Xaw/SmeLine.h>
	#include <X11/Xaw/Paned.h>
	#define protect_close(w) \
			Atom wm_delete_window = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", False); \
		XSetWMProtocols(XtDisplay(w), XtWindow(w), &wm_delete_window, 1)
#elif defined(QT5)
	#include <QtWidgets/QApplication>
	#include <QtWidgets/QWidget>
	#include <QtWidgets/QMainWindow>
	#include <QtWidgets/QBoxLayout>
	#include <QtWidgets/QLineEdit>
	#include <QtWidgets/QLabel>
	#include <QtWidgets/QPushButton>
	#include <QtWidgets/QGroupBox>
	#include <QtWidgets/QListWidget>
	#include <QtWidgets/QComboBox>
	#include <QtWidgets/QRadioButton>
	#include <QtWidgets/QCheckBox>
	#include <QtWidgets/QMessageBox>
	#include <QtWidgets/QInputDialog>
	#include <QtWidgets/QFileDialog>
	#include <QtWidgets/QMenuBar>
	#include <QtWidgets/QMenu>
	#include <QtWidgets/QWidgetAction>

	class GtxQApp : public QApplication {
		public:
			GtxQApp(int& argc, char* argv[]) : QApplication(argc, argv) {
			}

			bool notify(QObject* receiver, QEvent* event) {
				try {
					return QApplication::notify(receiver, event);
				}
				catch(const std::exception& ex) {
					printf("Exception thrown: %s\n", ex.what());
				}
				return true;
			}
	};
#else
	#include <gtk/gtk.h>
	#include <gmodule.h>
	#if GTK_CHECK_VERSION(3,0,0) 
		#define GTK3
	#elif GTK_CHECK_VERSION(2,0,0)
		#define GTK2
	#else
		#define GTK1
	#endif
	#ifdef GLADE
		#if defined(GTK3)
			#include <gladeui/glade.h>
		#else
			#include <glade/glade.h>
		#endif
	#endif
#endif

/***************************
* Global types and constants
*/
#define GTX_LABEL_WORDWRAP_COLUMN 45
#define GtxBitmap_width 32
#define GtxBitmap_height 32

#ifdef XAW
	#define GtxWidget Widget
#elif defined(QT5)
	#define GtxWidget QWidget*
#else
	#define GtxWidget GtkWidget*
#endif

/**
 * All widget creation functions return an unsigned int
 */
typedef unsigned int GtxWidgetId;

/**
 * Event function typedef
 */
typedef void (*GtxEvent)(unsigned int, char*, unsigned int, void*);

/**
 * Callback function
 */
typedef void (*GtxCallback)(GtxWidget, void*, void*);

/**
 * Return type for command and entry callbacks
 */
typedef struct {
	unsigned int id; ///< widget id
	GtxEvent fn; ///< pointer to callback function
	char* name; ///< widget name
	void* data; ///< data pointer returned to the app
	void* pdata; ///< private data pointer used by toolkit
	GtxCallback pfn; ///< private callback pointer user by TK
} GtxReturn;

/// Macros
/// Always get 1 more byte for \0
#define GTX_STR_MALLOC(size)	((char*)malloc(sizeof(char*) * ((size) + 1)))
#define GTX_TYPE_MALLOC(t)		(t*)malloc(sizeof(t))
#define GTX_TO_LATIN1(u8)			unsigned char l1_##u8[u8 ? strlen(u8) + 1 : 1];\
															utf8_to_latin1((unsigned char*)u8, l1_##u8);
#define GTX_TO_UTF8(l1)				unsigned char u8_##l1[l1 ? 1 + strlen(l1) * 2\
															: 1]; latin1_to_utf8((unsigned char*)l1, u8_##l1);
#define GTX_NOT_NULL(c, r)		if(NULL == (c)) r;
#define GTX_DBG(f, ...)				if(gdebug) printf("* %s +%d (%s) " \
															f, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#define GTX_ERR(f, ...)				fprintf(stderr, "* %s +%d (%s) " \
															f, __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#ifdef XAW
	#define gtx_sig_conn(w,s,c,d) XtAddCallback((w),XtNcallback,(c),(d))
#elif defined(GTK1)
	/// Signal connect uses different macros
	#define gtx_sig_conn(w,s,c,d) gtk_signal_connect(GTK_OBJECT(w), (s), GTK_SIGNAL_FUNC(c), (d))
	#define gtx_sig_conn_obj(w,s,c,d) \
		gtk_signal_connect_object(GTK_OBJECT(w), (s), GTK_SIGNAL_FUNC(c), GTK_OBJECT(d))
	/// Object
	#define GtxObject GtkObject
	/// Glade stuff
	#define gtx_glade_xml_new(f) glade_xml_new((f), NULL)
#elif defined(GTK2) || defined(GTK3)
	#define gtx_sig_conn(w,s,c,d) g_signal_connect(G_OBJECT(w), (s), G_CALLBACK(c), (d))
	#define gtx_sig_conn_obj(w,s,c,d) \
		g_signal_connect_swapped(G_OBJECT(w), (s), G_CALLBACK(c), G_OBJECT(d))
	#define GtxObject GObject
	#define gtx_glade_xml_new(f) glade_xml_new((f), NULL, NULL)
#else // QT5
	#define gtx_sig_conn(w,s,c,d) ((s*)w)->signal_connect(d)
#endif

/// function prototypes
#ifdef __cplusplus
extern "C" {
#endif

void				latin1_to_utf8(unsigned char* in, unsigned char* out);
void				utf8_to_latin1(unsigned char* in, unsigned char* out);
GtxWidgetId regWidget(GtxWidget w);
GtxReturn*	createReturn(GtxEvent fn, char *name, GtxWidget w, void *data,
                         void* pdata, GtxCallback pfn);
GtxReturn*  getReturnData(GtxWidget);
void				superCallback(GtxWidget w, void* data, void* clientData);
#ifdef XAW
void				superAction(Widget w, XEvent *event, String *params,
                        Cardinal *num_params);
void				quit(Widget w, XEvent *event, String *params, Cardinal *num_params);
#endif

#if defined(GTK1) || defined(GTK2)
void				quit(GtkWidget *w, gpointer data);
#endif

#ifdef GLADE
void				gladeXMLConnectFunc(const gchar *handler_name, GtxObject *object,
																const gchar *signal_name, const gchar *signal_data,
																GtxObject *connect_object, gboolean after,
																gpointer user_data);
#endif
#ifdef __cplusplus
}
#endif

/// Special QT5 macros
#ifdef QT5 
	#define GTX_CONFIG_WIDGET(g, s, n, q, p) \
	class g : public q { \
	private: \
		GtxReturn* data = NULL; \
	public: \
		g() : q() { \
			connect(this, static_cast<void (q::*)(p)>(&q::s), \
              this, static_cast<void (g::*)(p)>(&g::callSuperCB)); \
		} \
		void callSuperCB(p) { \
			GTX_DBG("callSuperCB: %s %x\n", n, this->data); \
			if(this->data) superCallback(this, this->data, NULL); \
		} \
		void signal_connect(GtxReturn* data) { \
			GTX_DBG("connecting data 0x%x\n", data); \
			this->data = data; \
		} \
	};
#else
	#define GTX_CONFIG_WIDGET(g, s, n, q, p) const char* g = n;
#endif

#endif /* #define __GTX_H__ */
