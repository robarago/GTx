#ifdef SWIG
%module pygtx
%{
#include "pygtx.h"

PyObject** gtxCallback;
unsigned int gtxCBCount = 0;
PyObject* gtxCallbackFunc;

/**
 * Static converter to allow NULL pointer to Py_None automatic conversion
 */
static PyObject* null2none(void* data) {
	if(NULL == data) return Py_BuildValue(""); // Safe ref Py_None
	else return Py_BuildValue("O", data);
}

/**
 * This function should match the prototype of the GtxEvent C callback:
 * C: typedef void (*GtxEvent)(unsigned int, char*, unsigned int, void*);
 * Python: def fun(w, name, data) */
static void PythonCallBack(unsigned int w, char* name, unsigned int lname,
	void* data)
{
	PyObject* func = gtxCallback[w];
	if(!PyCallable_Check(func)) {
		PyErr_SetString(PyExc_TypeError, "funtion not callable!");
		return;
	}
	printf("# %s +%d (%s) Event from widget# %d (%s) to pycall %p and data=%p\n",
		__FILE__, __LINE__, __func__, w, name, func, data);
	PyObject* arglist = Py_BuildValue("is#O&", w, name, lname, null2none, data);
	printf("# %s +%d (%s) Arglist : %p\n",
		__FILE__, __LINE__, __func__, arglist);
	PyGILState_STATE gstate = PyGILState_Ensure();
	PyObject* result = PyObject_Call(func, arglist, NULL);
	Py_DECREF(arglist);
	if(result == NULL) {
		PyErr_PrintEx(1);
		PyGILState_Release(gstate);
		return;
	}
	Py_DECREF(result);
	PyGILState_Release(gstate);
}
%}

%include "cstring.i"
%include "typemaps.i"

%typemap(in) GtxEvent fn {
	if(Py_None != $input) {
		if(!PyCallable_Check($input)) {
		PyErr_SetString(PyExc_TypeError, "Need a callable object!");
			return NULL;
		}
		gtxCallbackFunc = $input;
		$1 = PythonCallBack;
	}
}
%typemap(out) GtxWidgetId {
	if(gtxCallbackFunc != NULL) {
		gtxCallback = (PyObject**)realloc(gtxCallback, ($1 + 1) * sizeof(PyObject*));
		gtxCallback[$1] = gtxCallbackFunc;
		//printf("# %s +%d (%s) Registered PyCallable 0x%x with widget# %d\n",
		//	__FILE__, __LINE__, __func__, gtxCallbackFunc, $1);
		gtxCallbackFunc = NULL;
	}
	$result = PyInt_FromLong($1);
}
%typemap(in) void* data {
	$1 = (void*)$input;
}
%typemap(in) (unsigned int debug, int argc, char** argv) {
	int i, pargc;
	wchar_t** pargv;
	Py_GetArgcArgv(&pargc, &pargv);
	//printf("argc = %d, argv = %x\n", pargc, pargv);
	$1 = PyInt_AsLong($input);
	$2 = pargc;
	$3 = (char**)pargv;
}

%pythonbegin %{
def _module_available(tk):
  import os
  import sys
  for p in sys.path:
    if not p.endswith('.egg'):
      l = '%s/libGT%s.so' % (p, tk)
      if os.path.isfile(l):
        return l
  return None

def _preload_tk(debug, l):
  try:
    from ctypes import CDLL, RTLD_GLOBAL
    if debug:
      print('# Loading support lib from %s' % l)
    CDLL(l, RTLD_GLOBAL)
  except Exception as e:
    print('# Failed to preload support lib %s' % e)

def _preload_support_so():
  import sys
  required = None
  libs = []
  debug = False
  if '--debug-pygtx' in sys.argv:
    sys.argv.remove('--debug-pygtx')
    debug = True
  # first, check available tks
  for tk in ['xaw', 'gtk1', 'gtk2', 'qt5']:
    lib = _module_available(tk)
    if lib:
      if debug:
        print('# `%s\' lib available.' % tk)
      libs.append(lib)
      s = '--preload-GT%s' % tk
      if s in sys.argv:
        sys.argv.remove(s)
        required = lib
  # now, try to load one library
  if len(libs) == 0: 
    print('# No support libs available - Did you installed GTx ?')
  # check if command line specified TK is available
  elif required is not None:
    _preload_tk(debug, required)
    if debug:
      print('# Loaded required lib %s' % required)
  # try to load first found library
  else:
    if debug:
      print('# Trying first TK available: %s' % libs[0])
    _preload_tk(debug, libs[0])

_preload_support_so()
%}
%cstring_bounded_output(char* outtext, PATH_MAX);
#endif

#ifndef __PYGTX_H__
#define __PYGTX_H__
typedef unsigned int GtxWidgetId;
typedef void (*GtxEvent)(unsigned int, char*, unsigned int, void*);

extern void MainLoop();
extern unsigned int MainInit(char* classname, char* title, unsigned int debug, int argc, char** argv);

extern GtxWidgetId Button(int parent, char* label, GtxEvent fn, void* data);
extern GtxWidgetId List(int parent, unsigned int columns, GtxEvent fn, void* data);
extern GtxWidgetId ComboList(int parent, int width, int height, GtxEvent fn, void* data);
extern GtxWidgetId MenuItem(int menu, char* label, GtxEvent fn, void* data);
extern GtxWidgetId Option(int parent, char* label, unsigned int checked, int group, GtxEvent fn, void* data);
extern GtxWidgetId Check(int parent, char* label, unsigned int checked, GtxEvent fn, void* data);
extern GtxWidgetId Entry(int parent, char* label, int maxlength, char* text, GtxEvent fn, void* data);
enum DialogType    {  DIALOG_INFO,  DIALOG_QUESTION, DIALOG_WARNING, DIALOG_ERROR };
enum DialogButtons {  BUTTON_YES=1, BUTTON_NO,       BUTTON_OK,      BUTTON_CANCEL,
                      BUTTON_RETRY, BUTTON_IGNORE,   BUTTON_ABORT };
enum DialogMode    {  DIALOG_OPEN,  DIALOG_SAVE };
extern GtxWidgetId Dialog(int type, unsigned long buttonconf, char* title, char* msg, GtxEvent fn);
extern GtxWidgetId InputDialog(char* title, char* msg, char* text, GtxEvent fn);
extern GtxWidgetId FileDialog(char* title, int mode, char* filename, char* filter, GtxEvent fn);
extern void closeDialog(int d);
extern void getDialogSelection(int d, char* outtext);
enum Orientation { VERTICAL, HORIZONTAL };
extern unsigned int Box(int parent, int orientation, int innerspace);
extern unsigned int Frame(int parent, char* title, unsigned int width);
extern void addItem(int list, char* item);
extern void getSelectedItem(int list, char* outtext, int* OUTPUT);
extern void getComboListText(int combolist, char* outtext, unsigned int maxlength);
extern unsigned int Popup(int box, char* label);
extern unsigned int MenuBar(int window);
extern unsigned int Menu(int menubar, char* label);
extern void Separator(int menu);
extern unsigned int isSelected(int widget);
extern unsigned int Label(int parent, char* label);
extern void setLabelText(int label, char* text);
extern void getEntryText(int entry, char* outtext, int maxlength);
extern void setEntryText(int entry, char* text);
#endif // #define __PYGTX_H__
