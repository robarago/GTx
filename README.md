# GTx
GTx (AKA *Graphical Toolkit x*) consist of several
graphical user interface libraries (bindings) for GNU/Modula2
and Python.

## Description
GTx provides a single and straightforward linking interface
to the look and feel of Xaw7 (X Athena Widget version 7),
GTK+ 1.2, GTK+ 2.0 and Qt5 toolkits. Coded user interface
might be linked (or run, in the case of python3) on one or
more toolkit flavors without having to change a line of
code.

## Requisites
To get started you will need, GNU/Modula 2
([gm2](http://www.nongnu.org/gm2/about.html)) or Python.
Latest versions are 13.2.1 and 3.12.0 respectivelly, although
older versions should work (not tested).

Depending of what bindings you want to use, you will need,
additionally, Xaw7, GTK+ 1.2, GTK+ 2.0, Glade and/or Qt 5.1.0
libraries.

Refer to you distribution manual to find and install these
libraries.

If you are using F39, you can jumpt now to the section
[Installing in Fedora](#installing-in-fedora-39). If you use
other distribution, it might be helpful too.

## Building GTx
If you only want to compile and link with GTx,
you will only need runtime libraries, but if you prefer to
build the libraries by yourself, you'll nee the development
versions of the toolkit libraries and, of course your
build-essentials packages. Glade libraries are optional.
_It's very important to install the autotools package with
extended macros (autoconf-archive)_. Please, check your
distribution manual to find out the correct package name
that includes the macro `ax_pkg_swig` macro. More detail
in the section [Installing in Fedora](#installing-in-fedora-39).

## How to use
Once the required compiler (gm2) or interpreter (Python)
and the required graphic libraries are installed, you should
install or build GTx from source before they can be used.

### Clone

```shell
$ git clone https://gitlab.com/robarago/GTx
```

### Generate 'configure'

```shell
$ cd GTx/lib
$ ./bootstrap
libtoolize: putting auxiliary files in '.'.
libtoolize: copying file './ltmain.sh'
libtoolize: putting macros in AC_CONFIG_MACRO_DIRS, 'm4'.
libtoolize: copying file 'm4/libtool.m4'
libtoolize: copying file 'm4/ltoptions.m4'
libtoolize: copying file 'm4/ltsugar.m4'
libtoolize: copying file 'm4/ltversion.m4'
libtoolize: copying file 'm4/lt~obsolete.m4'
configure.ac:14: installing './compile'
configure.ac:9: installing './missing'
Makefile.am: installing './depcomp'
```

### Configure

```shell
$ ./configure
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
...
...
...
configure: creating ./config.status
config.status: creating GTxaw.pc
config.status: creating GTgtk1.pc
config.status: creating GTgtk2.pc
config.status: creating GTqt5.pc
config.status: creating Makefile
config.status: creating config.h
config.status: executing depfiles commands
config.status: executing libtool commands
+----------------------------------------------------+
|                                                    |
|  Building M2Gtx 0:0:0                              |
|  Summary: libraries that will be built             |
|  ================================================  |
|  pygtx     Python 2.7 gtx module                   |
|  libGTxaw  Athena Widgets (Xaw)                    |
|  libGTgtk1 GTK+ 1.2 Widgets WITH Glade support     |
|  libGTgtk2 GTK+ 2.0 Widgets WITH Glade support     |
|  libGTqt5  QT5 Toolkit support                     |
|                                                    |
+----------------------------------------------------+
```

### make

```shell
$ make
CDPATH="${ZSH_VERSION+.}:" && cd . && /bin/sh /GTx/lib/missing aclocal-1.15 -I m4
 cd . && /bin/sh /GTx/lib/missing automake-1.15 --gnu
CDPATH="${ZSH_VERSION+.}:" && cd . && /bin/sh /GTx/lib/missing autoconf
/bin/sh ./config.status --recheck
...
```

Check that the libs have been successfully built:

```shell
$ ls src/.libs/*.so src/.libs/*.a src/.libs/*.py
src/.libs/libGTgtk1.a   src/.libs/libGTqt5.a   src/.libs/_pygtx.a
src/.libs/libGTgtk1.so  src/.libs/libGTqt5.so  src/.libs/pygtx.py
src/.libs/libGTgtk2.a   src/.libs/libGTxaw.a   src/.libs/_pygtx.so
src/.libs/libGTgtk2.so  src/.libs/libGTxaw.so
```

### make examples
Now, you can try some or all the bundled examples.

```shell
$ cd ../share/GTx/examples
$ make
Making for all available toolkits: xaw qt5 gtk2 gtk1
make TK=xaw; make TK=qt5; make TK=gtk2; make TK=gtk1;
mkdir obj
mkdir xaw
gm2 -c -g -I../../../include  gtxdemo.mod -o obj/gtxdemo.o
...
```

Example binaries will be generated in a separate folder with the name of the
toolkit of the toolkit which the program have been linked with. So, for example,
you'll find `Xaw/gtxdemo` for the `gtxdemo` linked with Xaw and so on.

Run and play!

## Installing in Fedora 39
This is the list of packages you'll need in Fedora 39.
Following also de optional toolkit development libs.

- swig
- automake
- autoconf
- binutils-x86\_64-linux-gnu
- check-devel (will pull subunit-devel)
- libtool (will pull `m4` package if not installed)
- autoconf-archive (macro pagkages including `ax_pkg_swig`

### GNU Portable threads lib (not in F39 repo)
To be able to compile and link some programs with GM2,
you'll have to install manually one package, `pth`.

Unfortunatelly, F39 has no such package yet in their repos,
but it can be downloaded and installed from a rpm package.

The tested package is `pth-2.0.7-28.fc26.x86_64.rpm` although
it is possible to install from source by downloading the
sources from the [GNU Portable Threads](http://www.gnu.org/software/pth)
homepage.

If you opt to install the RPM version, note that it won't
work out of the box. You'll require to create a soft link
to the library in order to compile. Just issue this command:

```shell
$ sudo ln -s libpth.so.20.0.27 libpth.so
```

And it'll work (hopefully).

### Xaw
If you want to link with Xaw, you have to install the
package called `libXaw-devel` in F39. These packages will
be pulled when installed.

- libICE-devel
- libSM-devel
- libX11-devel
- libXau-devel
- libXext-devel
- libXmu-devel
- libXpm-devel
- libXt-devel
- libxcb-devel
- xorg-x11-proto-devel

### Gtk 1.2.10
If you want to link with GTK+ 1.2 (1.2.10), you have to install the
package called `gtk+-devel` in F39. These packages will
be pulled when installed.

- glib
- glib-devel
- libICE-devel
- libSM-devel
- gtk+
- libXfixes-devel
- libXi-devel
- libXt-devel

Additionally, if you want Glade support, you need to compile
find and manually compile `libglade-0.17`
(https://download.gnome.org/sources/libglade/0.17/libglade-0.17.tar.gz).

In order to make this package you'll need to install lib64xml1 and
lib64xml1-devel (not from F39 repos).

Also, if you want to design new Glade interfaces (using XML), you'll
need to compile the Glade 1 Designer Tool (`glade-0.6.4.tar.gz`) manually.
You don't need Gnome, Bonobo, Oaf or Gnome DB support, but it requires
rarian-compat (rarian, tinyxml) under F39 to configure and compile.

### Gtk2
If you want to link with GTK2, you have to install the
package called `gtk2-devel` in F39. These packages will
be pulled when installed:

- atk-devel
- bzip2-devel
- cairo-devel
- expat-devel
- fontconfig-devel
- freetype-devel
- gdk-pixbuf2-devel
- glib2-devel
- graphite2-devel
- harfbuzz-devel
- libXcomposite-devel
- libXcursor-devel
- libXft-devel
- libXinerama-devel
- libXrandr-devel
- libXrender-devel
- libicu-devel
- libpng-devel
- pango-devel
- pcre-cpp
- pcre-devel
- pcre-utf16
- pcre-utf32
- pixman-devel

Additionally, if you want Glade2 support, install
`libglade2-devel` package, that will install, these
dependencies:

- cmake-filesystem
- libxml2-devel
- xz-devel

### Gtk3
If you want to link with GTK3, you have to install the
package called `gtk3-devel` in F39. These packages will
be pulled when installed:

- at-spi2-atk-devel
- at-spi2-core-devel
- cairo-gobject-devel
- dbus-devel
- libepoxy-devel
- libxkbcommon-devel
- mesa-libwayland-egl-devel
- wayland-devel
- wayland-protocols-devel

Additionally, if you want Glade3 support, install
`glade-devel` package.


## Qt5
If you want to link with Qt5, you have to install the
package called `qt5-qtbase-devel` in F39. These packages
will be pulled when installed:

- cmake
- cmake-data
- cmake-rpm-macros
- jsoncpp
- libXdamage-devel
- libXxf86vm-devel
- libdrm-devel
- libglvnd-core-devel
- libglvnd-devel
- libglvnd-opengl
- libuv
- mesa-libEGL-devel 
- mesa-libGL-devel
- qt5-rpm-macros
- rhash

Drop [me](mailto:robarago@gmail.com) a note if you feel
in trouble.

Good luck!
